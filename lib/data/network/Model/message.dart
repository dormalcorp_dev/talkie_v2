import 'Podcasts.dart';

class Messagee {
  List<Messages> data;
  Pagination pagination;

  Messagee({this.data, this.pagination});

  Messagee.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Messages>();
      json['data'].forEach((v) {
        data.add(new Messages.fromJson(v));
      });
    }
    pagination = json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.pagination != null) {
      data['pagination'] = this.pagination.toJson();
    }
    return data;
  }
}

class Messages {
   int id;
  String photo;
  String nom;
  String date;
  bool notif;

  Messages({this.id, this.photo, this.nom, this.date, this.notif});

  Messages.fromJson(Map<String, dynamic> json) {
      id = json['id'];
    photo = json['photo'];
    nom = json['nom'];
    date = json['date'];
    notif = json['notif'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
        data['id'] = this.id;
    data['photo'] = this.photo;
    data['nom'] = this.nom;
    data['date'] = this.date;
    data['notif'] = this.notif;

    return data;
  }
}


