class User {
  String error;
  UserData data;

  User({this.error, this.data});

  User.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    data = json['data'] != null ? new UserData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class UserData {
  int id;
  String nom;
  String prenom;
  String photo;
  String pays;
  String ville;
  String lat;
  String lng;
  String username;
  String email;
  int nbrAmi;
  String tel;
  String date_nais;
  String prefixe_tel;
  String code_pays_tel;
  bool value=false;
  UserData(
      {this.id,
      this.nom,
      this.prenom,
      this.photo,
      this.pays,
      this.ville,
      this.lat,
      this.lng,
      this.username,
      this.email,
      this.nbrAmi,
      this.tel,
      this.date_nais,
      this.prefixe_tel,
      this.code_pays_tel,
        this. value,

      });

  UserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nom = json['nom'];
    prenom = json['prenom'];
    photo = json['photo'];
    pays = json['pays'];
    ville = json['ville'];
    lat = json['lat'];
    lng = json['lng'];
    username = json['username'];
    email = json['email'];
    nbrAmi = json['nbr_ami'];
     tel = json['tel'];
     date_nais=json['date_nais'];
     prefixe_tel=json['prefixe_tel'];
     code_pays_tel=json['code_pays_tel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nom'] = this.nom;
    data['prenom'] = this.prenom;
    data['photo'] = this.photo;
    data['pays'] = this.pays;
    data['ville'] = this.ville;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['username'] = this.username;
    data['email'] = this.email;
    data['nbr_ami'] = this.nbrAmi;
    data['tel'] = this.tel;
    data['date_nais']=this.date_nais;
    data['prefixe_tel']=this.prefixe_tel;
    data['code_pays_tel']=this.code_pays_tel;
    return data;
  }
}
