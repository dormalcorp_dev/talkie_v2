class DetailGroupe {
  InfosGroupe data;

  DetailGroupe({this.data});

  DetailGroupe.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new InfosGroupe.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class InfosGroupe {
  int id;
  String photo;
  String nom;
  int nbrParticipant;
  List<Participant> participant;

  InfosGroupe(
      {this.id, this.photo, this.nom, this.nbrParticipant, this.participant});

  InfosGroupe.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    photo = json['photo'];
    nom = json['nom'];
    nbrParticipant = json['nbrParticipant'];
    if (json['participant'] != null) {
      participant = new List<Participant>();
      json['participant'].forEach((v) {
        participant.add(new Participant.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['photo'] = this.photo;
    data['nom'] = this.nom;
    data['nbrParticipant'] = this.nbrParticipant;
    if (this.participant != null) {
      data['participant'] = this.participant.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Participant {
  String nom;
  String photo;
  int id;
  Participant({this.nom, this.photo, this.id});

  Participant.fromJson(Map<String, dynamic> json) {
    nom = json['nom'];
    photo = json['photo'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nom'] = this.nom;
    data['photo'] = this.photo;
    data['id'] = this.id;
    return data;
  }
}
