class Data {
  String error;
  String data;
  Socket socket;
  String call_id;
  Data({this.error, this.data,this.socket,this.call_id});

  Data.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    call_id = json['call_id'];
    data = json['data'];
        socket =
        json['socket'] != null ? new Socket.fromJson(json['socket']) : null;

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['data'] = this.data;
    data['call_id'] = this.call_id;
     if (this.socket != null) {
      data['socket'] = this.socket.toJson();
    }
    return data;
  }
  
}
class Socket {
  Message message;
  Conversation conversation;

  Socket({this.message, this.conversation});

  Socket.fromJson(Map<String, dynamic> json) {
    message =
        json['message'] != null ? new Message.fromJson(json['message']) : null;
    conversation = json['conversation'] != null
        ? new Conversation.fromJson(json['conversation'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.message != null) {
      data['message'] = this.message.toJson();
    }
    if (this.conversation != null) {
      data['conversation'] = this.conversation.toJson();
    }
    return data;
  }
}

class Message {
  int id;
  String voice;
  String sender;
  bool currentUser;
  String senderPhoto;
  String vuPar;
  String date;
  String duree;
  String type;
  String message;
    bool missed_call;

  int convId;
  List<String> receiverId;
  int senderId;

  Message(
      {this.id,
      this.voice,
      this.sender,
      this.currentUser,
      this.senderPhoto,
      this.vuPar,
      this.date,
      this.duree,
      this.convId,
      this.message,
      this.missed_call,
      this.type,
      this.receiverId,
      this.senderId});

  Message.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    voice = json['voice'];
    sender = json['sender'];
    currentUser = json['current_user'];
    senderPhoto = json['sender_photo'];
    vuPar = json['VuPar'];
    date = json['date'];
    duree = json['duree'];
    convId = json['conv_id'];
    receiverId = json['receiver_id'].cast<String>();
     senderId = json['sender_id'];
         type=json['type'];
    message=json['message'];
    missed_call=json["missed_call"];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['voice'] = this.voice;
    data['sender'] = this.sender;
    data['current_user'] = this.currentUser;
    data['sender_photo'] = this.senderPhoto;
    data['VuPar'] = this.vuPar;
    data['date'] = this.date;
    data['duree'] = this.duree;
    data['conv_id'] = this.convId;
    data['receiver_id'] = this.receiverId;
    data['sender_id'] = this.senderId;
         data['type'] = this.type;
     data['message'] = this.message;
     data['missed_call'] = this.missed_call;

    return data;
  }
}

class Conversation {
  int id;
  String photo;
  String nom;
  String date;
  bool notif;

  Conversation({this.id, this.photo, this.nom, this.date, this.notif});

  Conversation.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    photo = json['photo'];
    nom = json['nom'];
    date = json['date'];
    notif = json['notif'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['photo'] = this.photo;
    data['nom'] = this.nom;
    data['date'] = this.date;
    data['notif'] = this.notif;
    return data;
  }
}
