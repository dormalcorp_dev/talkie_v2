class Filtre {
  FiltreAudio data;

  Filtre({this.data});

  Filtre.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new FiltreAudio.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class FiltreAudio {
  String url;
  int tempData;

  FiltreAudio({this.url, this.tempData});

  FiltreAudio.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    tempData = json['tempData'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this.url;
    data['tempData'] = this.tempData;
    return data;
  }
}
