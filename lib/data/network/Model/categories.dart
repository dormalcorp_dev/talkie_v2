class Categories {
  String error;
  List<Categorie> data;

  Categories({this.error, this.data});

  Categories.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    if (json['data'] != null) {
      data = new List<Categorie>();
      json['data'].forEach((v) {
        data.add(new Categorie.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Categorie {
  int id;
  String nom_fr;
   String nom_en;
  String image;
  Categorie({this.id, this.nom_fr,nom_en, this.image});

  Categorie.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nom_fr = json['nom_fr'];
    nom_en = json['nom_en'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nom_fr'] = this.nom_fr;
    data['nom_en'] = this.nom_en;
    data['image'] = this.image;
    return data;
  }
}
