class Header {
  String error;
  HeaderData data;

  Header({this.error, this.data});

  Header.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    data = json['data'] != null ? new HeaderData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class HeaderData {
  String photo;
  String name;
  String nbrMessage;

  HeaderData({this.photo, this.name, this.nbrMessage});

  HeaderData.fromJson(Map<String, dynamic> json) {
    photo = json['photo'];
    name = json['name'];
    nbrMessage = json['nbr_message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['photo'] = this.photo;
    data['name'] = this.name;
    data['nbr_message'] = this.nbrMessage;
    return data;
  }
}