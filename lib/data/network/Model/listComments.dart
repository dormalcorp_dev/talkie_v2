import 'package:voicechanger/Page/commentsWidget.dart';
import 'package:voicechanger/data/network/Model/user.dart';

class ListComments {
  List<Comment> data;

  ListComments({this.data});

  ListComments.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Comment>();
      json['data'].forEach((v) {
        data.add(new Comment.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Comment {
  int id;
  UserData user;
  String file;
  String duree;
  bool liked;
  bool disliked;
  int nbrLike;
  int nbrDislike;

  PlayerState playerState = PlayerState.stopped;
  Comment({
    this.id,
    this.user,
    this.file,
    this.duree,
    this.liked,
    this.disliked,
    this.nbrLike,
    this.nbrDislike,
  });

  Comment.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    file = json['file'];
    duree = json['duree'];
    id = json['id'];
    liked = json['liked'];
    disliked = json['disliked'];
    nbrLike = json['nbr_like'];
    nbrDislike = json['nbr_dislike'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['file'] = this.file;
    data['duree'] = this.duree;
    data['id'] = this.id;
    data['liked'] = this.liked;
    data['disliked'] = this.disliked;
    data['nbr_like'] = this.nbrLike;
    data['nbr_dislike'] = this.nbrDislike;

    return data;
  }
}
