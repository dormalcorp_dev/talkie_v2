import 'package:voicechanger/Page/PodcastWidget.dart';

class Podcasts {
  List<PodcastsData> data;
  Pagination pagination;

  Podcasts({this.data, this.pagination});

  Podcasts.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<PodcastsData>();
      json['data'].forEach((v) {
        data.add(new PodcastsData.fromJson(v));
      });
    }
    pagination = json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.pagination != null) {
      data['pagination'] = this.pagination.toJson();
    }
    return data;
  }
}

class PodcastsData {
  int id;
  String createdAt;
  String duree;
  String nom;
  String description;
  bool liked;
  bool disliked;
  int nbrLike;
  int nbrDislike;
  int nbrComment;
  String file;
  UserPodcast user;
  PlayerState playerState=PlayerState.stopped;

  PodcastsData(
      {this.id,
      this.createdAt,
      this.duree,
      this.nom,
      this.description,
      this.liked,
      this.disliked,
      this.nbrLike,
      this.nbrDislike,
      this.nbrComment,
      this.file,
      this.user});

  PodcastsData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_At'];
    duree = json['dure'];
    nom = json['nom'];
    description = json['description'];
    liked = json['liked'];
    disliked = json['disliked'];
    nbrLike = json['nbr_like'];
    nbrDislike = json['nbr_dislike'];
    nbrComment = json['nbr_comment'];
    file = json['file'];
    user = json['user'] != null ? new UserPodcast.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_At'] = this.createdAt;
    data['dure'] = this.duree;
    data['nom'] = this.nom;
    data['description'] = this.description;
    data['liked'] = this.liked;
    data['disliked'] = this.disliked;
    data['nbr_like'] = this.nbrLike;
    data['nbr_dislike'] = this.nbrDislike;
    data['nbr_comment'] = this.nbrComment;
    data['file'] = this.file;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class UserPodcast {
  String nom;
  String prenom;
  String photo;
  int id;
  UserPodcast({this.nom, this.prenom, this.photo,this.id});

  UserPodcast.fromJson(Map<String, dynamic> json) {
    nom = json['nom'];
    prenom = json['prenom'];
    photo = json['photo'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nom'] = this.nom;
    data['prenom'] = this.prenom;
    data['photo'] = this.photo;
    data['id'] = this.id;
    return data;
  }
}

class Pagination {
  int pages;

  Pagination({ this.pages, });

  Pagination.fromJson(Map<String, dynamic> json) {
    pages = json['pages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pages'] = this.pages;
    return data;
  }
}
