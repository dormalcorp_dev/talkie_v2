import 'package:voicechanger/data/network/Model/Chaine.dart';

import 'Podcasts.dart';

class SearchUser {
  String error;
  List<UserChaine> data;
 Pagination pagination;
  int nbrTalkers;
  int nbrListeners;
  int nbrLike;
  int nbrDislike;

  SearchUser({this.error, this.data, this.pagination,this.nbrTalkers,this.nbrListeners,this.nbrLike,this.nbrDislike});

  SearchUser.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    if (json['data'] != null) {
      data = new List<UserChaine>();
      json['data'].forEach((v) {
        data.add(new UserChaine.fromJson(v));
      });
    }
    pagination = json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null;
            nbrTalkers = json['nbr_talkers'];
    nbrListeners = json['nbr_listeners'];
 nbrLike = json['nbr_like'];
    nbrDislike = json['nbr_dislike'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.pagination != null) {
      data['pagination'] = this.pagination.toJson();
    }
            data['nbr_talkers'] = this.nbrTalkers;
    data['nbr_listeners'] = this.nbrListeners;
 data['nbr_like'] = this.nbrLike;
    data['nbr_dislike'] = this.nbrDislike;
    return data;
  }
}



