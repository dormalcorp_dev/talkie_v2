import 'package:voicechanger/data/network/Model/Podcasts.dart';

class Chaine {
  UserChaine user;
  List<PodcastsData> data;
  int nbrTalkers;
  int nbrListeners;

  Chaine({this.user, this.data,this.nbrTalkers, this.nbrListeners});

  Chaine.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new UserChaine.fromJson(json['user']) : null;
    if (json['data'] != null) {
      data = new List<PodcastsData>();
      json['data'].forEach((v) {
        data.add(new PodcastsData.fromJson(v));
      });
    }
        nbrTalkers = json['nbr_talkers'];
    nbrListeners = json['nbr_listeners'];

    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
        data['nbr_talkers'] = this.nbrTalkers;
    data['nbr_listeners'] = this.nbrListeners;

    return data;
  }
}

class UserChaine {
  int id;
  String name;
  String username;
  String photo;
  int subscriber;
  bool my_profile;
  UserChaine({this.id, this.name, this.username, this.photo, this.subscriber,this.my_profile});

  UserChaine.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    photo = json['photo'];
    subscriber = json['subscriber'];
    my_profile = json['my_profile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['username'] = this.username;
    data['photo'] = this.photo;
    data['subscriber'] = this.subscriber;
    data['my_profile'] = this.my_profile;
    return data;
  }
}

class PodcastChaine {
  int id;
  String createdAt;
  String duree;
  String nom;
  String description;
  UserChaine user;

  PodcastChaine(
      {this.id,
      this.createdAt,
      this.duree,
      this.nom,
      this.description,
      this.user});

  PodcastChaine.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    duree = json['duree'];
    nom = json['nom'];
    description = json['description'];
    user = json['user'] != null ? new UserChaine.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['duree'] = this.duree;
    data['nom'] = this.nom;
    data['description'] = this.description;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}
