import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:voicechanger/Page/nouvelleDiscussionPage.dart';

class AppBarHeader extends StatelessWidget with PreferredSizeWidget {
  String name;
  bool actionTalk;
  bool buttonback;
  int currentindex;
  bool actionmessage;

  AppBarHeader({this.name, this.actionTalk,this.buttonback,this.actionmessage});


  @override
  Widget build(BuildContext context) {
 
    return AppBar(
          automaticallyImplyLeading: buttonback,
          elevation: 0,
          centerTitle: true,
        backgroundColor: Color(0xff25a996),
          title: 
          
            Text(name,
              style: TextStyle(
                fontFamily: 'Roboto',
                color: Color(0xffffffff),
                fontSize: 17,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              )),
         actions: <Widget>[
           if(actionmessage)     IconButton(icon: Icon(
      Icons.add,
      color: Colors.white,
      size: 26.0,
    ), onPressed: () {

Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => NouvelleDiscussionPage(), fullscreenDialog: true),
              );

    }),
            if(actionTalk)    IconButton( onPressed: () {
        Navigator.pushNamed(context, "/explorerPage").then((value) => {          
    Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false,
                  arguments: {'selectedindex': 1}),
});
      }, icon: SvgPicture.asset(
  "assets/icon/search.svg",
)),

          ],
        );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

}
