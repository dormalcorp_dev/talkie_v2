import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:voicechanger/app_localizations.dart';

class Bubble extends StatelessWidget {
  final Widget slider;
  final isMe, delivered;
  final String duree;
  final String date;
  Bubble({this.slider, this.delivered, this.isMe,this.duree,this.date});

  @override
  Widget build(BuildContext context) {
    final bg = isMe ? Color(0xff25a996) :Colors.white ;
    final style =isMe ? TextStyle(
		color: Colors.white,
		fontSize: 10,
		fontFamily: 'Ubuntu',
		fontWeight: FontWeight.w300,
	): 
  TextStyle(
	
    	color: Color(0xff949494),
		fontSize: 10,
		fontFamily: 'Ubuntu',
		fontWeight: FontWeight.w300,
	) ;
    final radius = BorderRadius.circular(4);
    return Column(
      children: <Widget>[
        Container(
           constraints: BoxConstraints(
    maxHeight: double.infinity,
    minHeight: 42
),
          margin: const EdgeInsets.all(3.0),
          decoration: BoxDecoration(
		boxShadow: [BoxShadow(
        color: Color(0x0c000000),
        offset: Offset(0,0),
        blurRadius: 20,
        spreadRadius: 0
    ) ],
            color: bg,
            borderRadius: radius,
          ),
          child: Stack(
            children: <Widget>[
              slider,
              Positioned(
                bottom: 1.0,
                left: 1.0,
                child:                     Text(duree??"",style: style),

                   
              ),
              Positioned(
                bottom: 1.0,
                right: 1.0,
                child: 
                     Text(getDate(DateTime.parse(date)??"",context),style: style),
              )
            ],
          ),
        )
      ],
    );
  }
}
 String getDate(DateTime date,context) {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    String finaldate;
    final aDate = DateTime(date.year, date.month, date.day);
    if (aDate == today) {
      finaldate = DateFormat("h:mm a").format(date.toLocal());
    } else if (aDate == yesterday) {
      finaldate = AppLocalizations.of(context).translate('yesterday');
    } else {
      finaldate = DateFormat("EEE, dd MMM").format(date.toLocal());
    }
    return finaldate;
  }