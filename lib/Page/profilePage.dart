
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/Constant.dart';

import 'package:voicechanger/data/network/ClientApi.dart';

import '../data/network/Model/user.dart';

import 'package:image_picker/image_picker.dart';
import 'dart:convert';



class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Profile();
  }
}

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

String _selectedDate;
bool errordate = false;
String indicatif;
UserData userData;
String code_pays_tel = "";

class _ProfileState extends State<Profile> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  String _base64Image;
  Future<User> user;
  Future<User> edit;
  FocusNode _focusNode = FocusNode();
  ImageProvider _image;

  TextEditingController usernameController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController surnameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  var selectedDate = DateTime.now();
  TextEditingController _date = new TextEditingController();
  var myFormat = DateFormat('dd-MM-yyyy');

  bool phone_error=false;
  bool email_error_taken=false;
  bool email_error_valid=false;
  bool username_error=false;

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901),
        lastDate: DateTime(2100));
    if (picked != null)
      setState(() {
        selectedDate = picked;

        _date.value = TextEditingValue(text: "${myFormat.format(picked)}");
      });
  }

  Future getImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);
    if (image != null) {
      List<int> imageBytes = image.readAsBytesSync();
      print(imageBytes);
      String base64Image = base64Encode(imageBytes);
      print(base64Image);

      setState(() {
        _image = FileImage(image);
        _base64Image = base64Image;
      });
    }
  }

  _getUser(String token) {
    // here you "register" to get "notifications" when the getEmail method is done.
    user = ClientApi.getUserDetail(token);
    user
        .then((value) => {
              setState(() {
                print(value.data);
                userData = value.data;
                usernameController..text = userData.username;
                nameController..text = userData.nom;
                surnameController..text = userData.prenom;
                emailController..text = userData.email;
                phoneController..text = userData.tel;
                _date.value = TextEditingValue(
                    text:
                        "${myFormat.format(DateTime.parse(userData.date_nais).toLocal())}");

                _image = NetworkImage(Endpoints.baseUrlImage + userData.photo);
                selectedDate = DateTime.parse(userData.date_nais);
                _selectedDate = userData.date_nais;
                indicatif = userData.prefixe_tel;
                code_pays_tel = userData.code_pays_tel;
              }),
            })
        .catchError((error) => {print("erreur : " + error.toString())});
  }

  @override
  void initState() {
    super.initState();
    errordate = false;
    getToken().then((cleapi) {
      // here you "register" to get "notifications" when the getEmail method is done.
      _getUser(cleapi);
    });

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  TextStyle style = TextStyle(
      fontFamily: 'Ubuntu',
      color: Color(0xffffffff),
      fontSize: 16,
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.normal,
      letterSpacing: 0);
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    final iconLogout = FlatButton(
      onPressed: () {
        showAlertDialog(context);
      }, // button pressed
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.power_settings_new, color: Colors.white),
          Text(
          AppLocalizations.of(context).translate('logout'),
            style: TextStyle(
                color: Colors.white, decoration: TextDecoration.underline),
          )
          // icon
        ],
      ),
    );

    final usernameField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            style: style,
            controller: usernameController,
            obscureText: false,
            decoration: InputDecoration(
              errorText: username_error ? AppLocalizations.of(context).translate('username_taken')  : null,
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('username'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final nameField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            style: style,
            controller: nameController,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('name'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));
    final surnameField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            style: style,
            controller: surnameController,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('surname'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final emailField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            style: style,
            controller: emailController,
            decoration: InputDecoration(
              errorText: email_error_taken ? AppLocalizations.of(context).translate('email_taken'): email_error_valid ?AppLocalizations.of(context).translate('email_invalid'):null,
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('email_adress'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));
    Widget _buildDropdownItem(Country country) => Container(
          child: Row(
            children: <Widget>[
              CountryPickerUtils.getDefaultFlagImage(country),
              Text("+${country.phoneCode}(${country.isoCode})"),
            ],
          ),
        );

    final phoneField = Container(
        margin: const EdgeInsets.fromLTRB(8, 0, 8, 0),
        decoration: new BoxDecoration(
            color: Color(0xff25a996),
            borderRadius: new BorderRadius.all(Radius.circular(15))),
        child: Row(children: <Widget>[
          CountryPickerDropdown(
            initialValue: 'FR',
            itemBuilder: _buildDropdownItem,
            priorityList: [
              CountryPickerUtils.getCountryByIsoCode('FR'),
              CountryPickerUtils.getCountryByIsoCode('DE'),
              CountryPickerUtils.getCountryByIsoCode('ES'),
              CountryPickerUtils.getCountryByIsoCode('BE'),
              CountryPickerUtils.getCountryByIsoCode('IT'),
            ],
            sortComparator: (Country a, Country b) =>
                a.isoCode.compareTo(b.isoCode),
            onValuePicked: (Country country) {
              print("${country.name}");
              setState(() => {
                    indicatif = "+" + country.phoneCode,
                    code_pays_tel = country.isoCode,
                  });
            },
          ),
          Expanded(
              child: TextField(
                  style: style,
                  controller: phoneController,
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    errorText: phone_error ? AppLocalizations.of(context).translate('phone_taken') : null,
                    border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white)),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white)),
                    disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white)),
                    labelText: AppLocalizations.of(context).translate('phone_number'),
                    labelStyle: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    ),
                  )))
        ]));

    final signupButon = Padding(
        padding: EdgeInsets.fromLTRB(38, 71, 37, 60),
        child: SizedBox(
          width: double.infinity, // match_parent
          height: 45,
          child: RaisedButton(
            elevation: 0,
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            onPressed: () {
              print(emailController.value);
              EasyLoading.instance
                ..loadingStyle = EasyLoadingStyle.custom
                ..backgroundColor = Colors.white
                ..progressColor = Colors.white
                ..indicatorType = EasyLoadingIndicatorType.wave
                ..indicatorColor = Color(0xff3bc0ae)
                ..textColor = Color(0xff3bc0ae)
                ..userInteractions = false
                ..maskType = EasyLoadingMaskType.black;

              EasyLoading.show(
                status: AppLocalizations.of(context).translate('loading'),
              );

              getToken().then((token) => {
                    edit = ClientApi.editProfile(
                        token,
                        usernameController.text,
                        emailController.text,
                        nameController.text,
                        surnameController.text,
                        passwordController.text,
                        selectedDate.month.toString() +
                            "/" +
                            selectedDate.day.toString() +
                            "/" +
                            selectedDate.year.toString(),
                        _base64Image,
                        nameController.text + ".png",
                        phoneController.text,
                        indicatif,
                        code_pays_tel),
                    edit
                        .then((value) => {
                              EasyLoading.dismiss(),
                              Fluttertoast.showToast(
                                  msg: AppLocalizations.of(context).translate('profile_edited'),
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                
                                  backgroundColor: Colors.black,
                                  textColor: Colors.white,
                                  fontSize: 16.0),
                            })
                        .catchError((error) => {
                              if (error.toString().contains(
                                  "This value is not a valid email address"))
                                
                                                                 {setState((){email_error_valid=true;})} else {setState((){email_error_valid=false;})},

                                
                              if (error
                                  .toString()
                                  .contains("Username Alrady taken"))
                             {setState((){username_error=true;})} else {setState((){username_error=false;})},

                              if (error
                                  .toString()
                                  .contains("Email Alrady taken"))
                                {setState((){email_error_taken=true;})} else {setState((){email_error_taken=false;})},
                              if (error
                                  .toString()
                                  .contains("Phone number already taken"))
                              {setState((){phone_error=true;}),} else {setState((){phone_error=false;})},

                              //emailController.addError('Wrong ID.'),
                              //passwordController.addError('Wrong Password'),

                              EasyLoading.dismiss(),
                              EasyLoading.instance.loadingStyle =
                                  EasyLoadingStyle.custom,
                              EasyLoading.instance.backgroundColor = Colors.red,
                              EasyLoading.instance.indicatorColor =
                                  Colors.white,
                              EasyLoading.instance.progressColor = Colors.white,
                              EasyLoading.instance.textColor = Colors.white,
                              EasyLoading.showError(error.toString())
                            }),
                  });
            },
            child: Text(AppLocalizations.of(context).translate('edit'),
                style: TextStyle(
                  fontFamily: 'Ubuntu',
                  color: Color(0xff25a996),
                  fontSize: 15,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                )),
          ),
        ));

    return Scaffold(
      backgroundColor: Color(0xff25a996),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xff25a996),
        centerTitle: true,
        actions: <Widget>[iconLogout],
      ),
      body: Stack(children: <Widget>[
        SingleChildScrollView(
          child: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onPanDown: (_) {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            behavior: HitTestBehavior.translucent,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          final act = CupertinoActionSheet(
                              actions: <Widget>[
                                CupertinoActionSheetAction(
                                  child: Text(AppLocalizations.of(context).translate('choose_photo'),
                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0xff262626),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  onPressed: () {
                                    Navigator.of(context, rootNavigator: true)
                                        .pop();

                                    getImage(ImageSource.gallery);
                                  },
                                ),
                                CupertinoActionSheetAction(
                                  child: Text(AppLocalizations.of(context).translate('take_picture'),
                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0xff262626),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      )),
                                  onPressed: () {
                                    Navigator.of(context, rootNavigator: true)
                                        .pop();
                                    getImage(ImageSource.camera);
                                  },
                                )
                              ],
                              cancelButton: CupertinoActionSheetAction(
                                child: Text(AppLocalizations.of(context).translate('cancel'),
                                    style: TextStyle(
                                      fontFamily: 'SFProText',
                                      color: Color(0xff262626),
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    )),
                                onPressed: () {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                },
                              ));
                          showCupertinoModalPopup(
                              context: context,
                              builder: (BuildContext context) => act);
                        },
                        child: Container(
                            margin: EdgeInsets.only(top: 18),
                            width: 100,
                            height: 100,
                            decoration: new BoxDecoration(
                              color: Color(0xa8000000),
                              borderRadius: BorderRadius.circular(8),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0x25000000),
                                    offset: Offset(0, 5),
                                    blurRadius: 15,
                                    spreadRadius: 0)
                              ],
                            ),
                            alignment: Alignment.center,
                            child: (_image != null)
                                ? Container(
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            fit: BoxFit.cover, image: _image)))
                                : Container()));
                  }),
                  
                  nameField,
                  surnameField,
                  usernameField,
                  emailField,
                  phoneField,
                  Padding(
                      padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: GestureDetector(
                        onTap: () => _selectDate(context),
                        child: AbsorbPointer(
                          child: TextFormField(
                              controller: _date,
                              style: style,
                              keyboardType: TextInputType.datetime,
                              decoration: InputDecoration(
                                border: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                disabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                labelText: AppLocalizations.of(context).translate('birthdate'),
                                labelStyle: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Color(0xffffffff),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                ),
                              )),
                        ),
                      )),
                  signupButon,
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  Future<void> logoutUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear().then((onValue) => {
      
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/login', (Route<dynamic> route) => false)
        });
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text( AppLocalizations.of(context).translate('no')),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(); // dismiss dialog
      },
    );
    Widget continueButton = FlatButton(
      child: Text( AppLocalizations.of(context).translate('yes')),
      onPressed: () {
        logoutUser();
        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(AppLocalizations.of(context).translate('logout')),
      content: Text(AppLocalizations.of(context).translate('confirmation_logout')),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }
}
