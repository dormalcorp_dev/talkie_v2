
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:file/local.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:voicechanger/Page/commentsWidget.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/listComments.dart';
import 'package:voicechanger/main.dart';

class CommentsPage extends StatefulWidget {

  final LocalFileSystem localFileSystem;
  CommentsPage({Key key, localFileSystem})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  Comments createState() => Comments();
}

class Comments extends State<CommentsPage> {
  
int id;
int id_user;
  Future<ListComments> commentData;
      var listcomment = [];

  @override
  void initState() {
    super.initState();
    getToken().then((token)=>{
      getAllComments(token,id)
    });

  }

  @override
  Widget build(BuildContext context) {
     Map arguments;
     
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print(arguments['id']);
      id = arguments['id'];
      id_user = arguments['id_user'];
    }
    return  Container(
            color:Colors.black,
          padding: EdgeInsets.only(top: 54),
          child: Material(

              color: Color(0xff25a996),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0)),
              child: 
               Stack(children: <Widget>[
                 Column(children: [
               Align(
                      alignment: Alignment.topLeft,
                      child:   Row(children: <Widget>[
                 IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 24.0,
                          ),
                          onPressed: () async {
                        
                              Navigator.pop(context);
                            
                          }),
                     Text( AppLocalizations.of(context).translate('comments') + " (${listcomment.length})",style: TextStyle(color:Colors.white ,fontSize: 18,fontFamily: "Helvetica",fontWeight: FontWeight.w400))])
             
                ),

  Expanded(
              child: ListView.builder(
                  itemCount:listcomment.length ,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext ctxt, int index) {
                    
               return   CommentWidget(listcomment[index],id_user,reset: ()=>reset(listcomment[index].id),refreshComment: ()=>
               { getToken().then((token)=>{
      getAllComments(token,id)
    })}
               
               );
                  }))


                ]),
                 Padding(
                 padding: EdgeInsets.only(bottom:40),
                 child: Align(

                          alignment: Alignment.bottomCenter,
                          child:IconButton(
                            iconSize: 57.12,
                            onPressed: () {
                                Navigator.pushNamed(context, "/addcomment", arguments: {
                        'id': id,
                      }).then((value) => {

                          getToken().then((token)=>{
      getAllComments(token,id)
    })
                      });
                            },
                            icon: SvgPicture.asset(
                              "assets/icon/group-2-copy-9.svg", color: Color(0xFFC4C4C4),
                            )))),
               ])),
              
        );
  }
reset(id){
for (int i = 0; i < listcomment.length; i++) {
  if(listcomment[i].playerState==PlayerState.playing)
  setState(() {
                          listcomment[i].playerState = PlayerState.stopped;

  });
                    
                  }
}


  getAllComments(String token, int id_podcast) {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    // here you "register" to get "notifications" when the getEmail method is done.
    commentData = ClientApi.getListComments(token, id_podcast);
    commentData
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                print(value.data);
                listcomment = value.data;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }



 
}


