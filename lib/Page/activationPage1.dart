import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/data.dart';

class Activate1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Activate1Page();
  }
}

class Activate1Page extends StatefulWidget {
  @override
  _Activate1PageState createState() => _Activate1PageState();
}

class _Activate1PageState extends State<Activate1Page>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  Future<Data> data;
  FocusNode _focusNode = FocusNode();
  TextEditingController emailController = new TextEditingController();
  TextStyle style = TextStyle(
    fontFamily: 'Ubuntu',
    color: Color(0xffffffff),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );
  bool is_checked = false;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final emailField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  is_checked = true;
                });
              } else {
                setState(() {
                  is_checked = false;
                });
              }
            },
            style: style,
            controller: emailController,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('email_adress'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final sendButon = Padding(
        padding: EdgeInsets.fromLTRB(38, 71, 37, 0),
        child: SizedBox(
          width: double.infinity, // match_parent
          height: 45,
          child: RaisedButton(
            elevation: 0,
            color: (is_checked) ? Colors.white : Color(0xff6cc5b8),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            onPressed: () {
              if (is_checked) {
                EasyLoading.instance
                  ..loadingStyle = EasyLoadingStyle.custom
                  ..backgroundColor = Colors.white
                  ..progressColor = Colors.white
                  ..indicatorType = EasyLoadingIndicatorType.wave
                  ..indicatorColor = Color(0xff3bc0ae)
                  ..textColor = Color(0xff3bc0ae)
                  ..userInteractions = false
                  ..maskType = EasyLoadingMaskType.black;
                EasyLoading.show(
                  status: AppLocalizations.of(context).translate('loading'),
                );

                data = ClientApi.activateCheckApi(emailController.text);
                data
                    .then((value) => {
                          EasyLoading.instance.loadingStyle =
                              EasyLoadingStyle.light,
                          EasyLoading.dismiss(),
                          Fluttertoast.showToast(
                              msg: AppLocalizations.of(context).translate('enter_code_email'),
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,
                              backgroundColor: Colors.black,
                              textColor: Colors.white,
                              fontSize: 16.0),
                          Navigator.pushNamed(context, "/activate")
                        })
                    .catchError((error) => {
                          EasyLoading.dismiss(),
                          EasyLoading.instance.loadingStyle =
                              EasyLoadingStyle.custom,
                          EasyLoading.instance.backgroundColor = Colors.red,
                          EasyLoading.instance.indicatorColor = Colors.white,
                          EasyLoading.instance.progressColor = Colors.white,
                          EasyLoading.instance.textColor = Colors.white,
                          EasyLoading.showError(AppLocalizations.of(context).translate('verify_email'))
                        });
              }
            },
            child: Text(AppLocalizations.of(context).translate('send'),
                style: TextStyle(
                  fontFamily: 'Ubuntu',
                  color: Color(0xff25a996),
                  fontSize: 15,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                )),
          ),
        ));

    final login = Padding(
        padding: EdgeInsets.fromLTRB(0, 15, 0, 60),
        child: Text.rich(
          TextSpan(
            text: AppLocalizations.of(context).translate('return_to'),
            style: TextStyle(
              fontFamily: 'Ubuntu',
              color: Color(0xffffffff),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
            children: <TextSpan>[
              TextSpan(
                  text: AppLocalizations.of(context).translate('login_screen'),
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () => Navigator.pushNamed(context, "/login"),
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    color: Color(0xffffffff),
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    decoration: TextDecoration.underline,
                  )),
              // can add more TextSpans here...
            ],
          ),
          overflow: TextOverflow.visible,
        ));
    return Scaffold(
      backgroundColor: Color(0xff25a996),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xff25a996),
      ),
      body: Stack(children: <Widget>[
        SingleChildScrollView(
          child: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onPanDown: (_) {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            behavior: HitTestBehavior.translucent,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 172.0,
                    child: Image.asset(
                      "assets/logo.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.only(top: 0, right: 52, left: 18),
                          child: Text("Activation",
                              style: TextStyle(
                                fontFamily: 'Salome',
                                color: Color(0xffffffff),
                                fontSize: 28,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding:
                              EdgeInsets.only(top: 15, right: 52, left: 18),
                          child: Text(
                             AppLocalizations.of(context).translate('enter_email_activation'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xffffffff),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  emailField,
                  SizedBox(
                    height: 25.0,
                  ),
                  sendButon,
                  SizedBox(
                    height: 25.0,
                  ),
                  login
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
