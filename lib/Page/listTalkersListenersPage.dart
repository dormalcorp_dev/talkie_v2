import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/Chaine.dart';
import 'package:voicechanger/data/network/Model/SearchUser.dart';

import 'AppBarHeader.dart';
import 'Footer.dart';
import 'discussionPage.dart';
import 'myWorldPage.dart';

class ListTalkersListenersPage extends StatefulWidget {
  ListTalkersListeners createState() => ListTalkersListeners();
}

class ListTalkersListeners extends State<ListTalkersListenersPage> {
  @override
  Widget build(BuildContext context) {
    Map arguments;
    String nom;
    int id;
    int page;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
        nom=arguments['username'];
        id=arguments['id'];
        page=arguments['page'];
    }
    return TalkersListeners(nom: nom, id: id,page:page);
  }
}

class TalkersListeners extends StatefulWidget {
  final String nom;
  final int id;
  final int page;

  TalkersListeners({Key key, this.id, this.nom,this.page});

  @override
  _TalkersListenersPageState createState() => _TalkersListenersPageState();
}

class _TalkersListenersPageState extends State<TalkersListeners> {
  Map arguments;
  int _selectedIndex = 1;
  Widget appBar;
  var listTalkers = new List<UserChaine>();
  Future<SearchUser> getlistTalkers;
    int   nbr_listeners;
    int nbr_talkers;

  
  var listListeners = new List<UserChaine>();
  Future<SearchUser> getlistListeners;
    bool show_talker=false;
bool show_listener=false;
  @override
  void initState() {
    super.initState();
        appBar = AppBarHeader(
        name: widget.nom,
        buttonback: true,
        actionTalk: false,
        actionmessage: false);
        getlistTalkersAPI(widget.id);
        getListListenersAPI(widget.id);
  }
 
  List<Widget> _widgetOptions;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;

      if (index == 0) {
        appBar = AppBarHeader(
            name: "Discussions",
            buttonback: false,
            actionTalk: false,
            actionmessage: true);
      }

      if (index == 1) {
        appBar = AppBarHeader(
            name: widget.nom,
            buttonback: true,
            actionTalk: false,
            actionmessage: false);
      }
      if (index == 2) {
        appBar = null;
      }
    });
    
  }


  @override
  Widget build(BuildContext context) {
  

       _widgetOptions = <Widget>[
      DiscussionPage(),
      DefaultTabController(
                initialIndex:widget.page,
      
        length: 2,
        child: Column(
          children: <Widget>[
            
            Container(
                            constraints: BoxConstraints.expand(height: 52),

              child: TabBar(
                
                labelColor:  Color(0xff25a996) ,
                unselectedLabelColor: Color(0xff9b9b9b),
              indicatorColor:Color(0xff25a996),
              labelStyle:TextStyle(
    fontFamily: 'Ubuntu',
    color: Color(0xff25a996),
    fontSize: 16,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    letterSpacing: 0.4,
    
    ),  //For Selected tab
              unselectedLabelStyle: TextStyle(
    fontFamily: 'Ubuntu',
    color: Color(0xff9b9b9b),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0.4,
    
    ), //For Un-selected Tabs

                tabs: [
                Tab(text: (nbr_listeners!=null)?"$nbr_listeners Listeners":""),
                Tab(text: (nbr_talkers!=null)?"$nbr_talkers Talkers":""),
              ]),
            ),
              

            Expanded(
              child: Container(
                child: TabBarView(children: [
                   (listListeners.isEmpty && show_listener)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Center(
                      child: Text(AppLocalizations.of(context).translate('empty_listeners'),
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff9b9b9b),
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ))),
                ],
              )
            :
                  Column(
                    children: <Widget>[
                       Container(
                              height: 36,margin: EdgeInsets.fromLTRB(8, 20, 8, 0),child:TextField(
                                      onChanged: (query) => {
                                           if (query != "")
                                              searchListeners(query,widget.id)
                                            else
                                              {
                                              getListListenersAPI(widget.id)
                                              }
                                          },
                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0x7f9b9b9b),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: -0.408,
                                      ),
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Color(0x129b9b9b),
                                          hintStyle: TextStyle(
                                            fontFamily: 'SFProText',
                                            color: Color(0x7f9b9b9b),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: -0.408,
                                          ),
                                          hintText: AppLocalizations.of(context).translate('search'),
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 0,
                                                  horizontal: 15.0),
                                          prefixIcon: Icon(Icons.search,
                                              color: Color(0xff949494)),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          disabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          )))),
                     
                     
                     
                  Expanded(child:Container(
                                  margin: EdgeInsets.fromLTRB(8, 15, 8, 0),

                        child:
           GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // number of items per row
              crossAxisCount: 3,
              // vertical spacing between the items
              mainAxisSpacing: 30.0,
              // horizontal spacing between the items
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            itemBuilder: (BuildContext context, int i) {
                return 
              UserWidget(listListeners[i]);
              
            },
            itemCount:
                listListeners.length ,
          )))]),
                     (listTalkers.isEmpty && show_listener)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Center(
                      child: Text(AppLocalizations.of(context).translate('empty_talkers'),
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff9b9b9b),
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ))),
                ],
              )
            :   Column(
                    children: <Widget>[
                       Container(
                              height: 36,margin: EdgeInsets.fromLTRB(8, 20, 8, 0),child:TextField(
                                      onChanged: (query) => {
                                           if (query != "")
                                              searchTalkers(query,widget.id)
                                            else
                                              {
                                              getlistTalkersAPI(widget.id)
                                              }
                                          },
                                      style: TextStyle(
                                        fontFamily: 'SFProText',
                                        color: Color(0x7f9b9b9b),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: -0.408,
                                      ),
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Color(0x129b9b9b),
                                          hintStyle: TextStyle(
                                            fontFamily: 'SFProText',
                                            color: Color(0x7f9b9b9b),
                                            fontSize: 17,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            letterSpacing: -0.408,
                                          ),
                                          hintText: AppLocalizations.of(context).translate('search'),
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 0,
                                                  horizontal: 15.0),
                                          prefixIcon: Icon(Icons.search,
                                              color: Color(0xff949494)),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          disabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: Color(0x129b9b9b)),
                                          )))),
                  Expanded(child:Container(
          margin: EdgeInsets.fromLTRB(8, 15, 8, 0),
          child: GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // number of items per row
              crossAxisCount: 3,
              // vertical spacing between the items
              mainAxisSpacing: 30.0,
              // horizontal spacing between the items
              crossAxisSpacing: 15.0,
              childAspectRatio: 1.0,
            ),
            itemBuilder: (BuildContext context, int i) {
                return 
                
              
                UserWidget(listTalkers[i]);
              
            },
            itemCount:
                listTalkers.length ,
          )))]
                                )]),
              ),
            )

          ],
        ),
      ),
      MyWorldPage(),
    ];
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        appBar: appBar,
        body: Container(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: Footer(_selectedIndex, _onItemTapped));
  }
  getlistTalkersAPI(int id) {
    getToken().then((cleapi) {
      getlistTalkers = ClientApi.getListTalkers(cleapi, id);
      getlistTalkers
          .then((value) => {
                setState(() {
                  listTalkers = value.data;
                  show_talker = true;
                  nbr_talkers=value.nbrTalkers;
                  nbr_listeners=value.nbrListeners;

                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }


   searchTalkers(String key,int id) {
    getToken().then((cleapi) {
      getlistTalkers = ClientApi.searchListTalkers(cleapi, id,key);
      getlistTalkers
          .then((value) => {
                setState(() {
                  listTalkers = value.data;
                  show_talker = false;
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }

    searchListeners(String key,int id) {
    getToken().then((cleapi) {
      getlistListeners = ClientApi.searchListListeners(cleapi, id,key);
      getlistListeners
          .then((value) => {
                setState(() {
                  listListeners = value.data;
                  show_listener = false;
                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }


    getListListenersAPI(int id) {
    getToken().then((cleapi) {
      getlistListeners = ClientApi.getListListeners(cleapi, id);
      getlistListeners
          .then((value) => {
                setState(() {
                  listListeners = value.data;
                    show_listener = true;

                })
              })
          .catchError((error) => {
                print("erreur : " + error.toString()),
              });
    });
  }
   UserWidget(UserChaine user) {
    return GestureDetector(
        onTap: () {
                                getID().then((id) {
                                                 
                                               
                      if(user.id!=id)

          Navigator.pushNamed(context, "/chainePage",
              arguments: {'id': user.id, 'username': user.name});
              });
        },
        child: Column(children: [
          if (user.photo != "")
            Container(
              width: 52,
              height: 52,
              decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x33000000),
                        offset: Offset(0, 0),
                        blurRadius: 5,
                        spreadRadius: 0)
                  ],
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(Endpoints.baseUrlImage + user.photo),
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8)),
            ),
          SizedBox(
            height: 4,
          ),
          Text(user.name,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xff3f3c3c),
                fontSize: 13,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.2708333333333333,
              )),
       
        ]));
  }

}

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("apikey");
}
  
  Future<int> getID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("id");
  }

 