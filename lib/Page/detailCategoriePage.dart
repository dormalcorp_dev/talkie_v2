import 'dart:io';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/AppBarHeader.dart';
import 'package:voicechanger/Page/MyWorldPage.dart';
import 'package:voicechanger/Page/discussionPage.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/Podcasts.dart';

import 'Footer.dart';
import 'PodcastWidget.dart';

class DetailCategoriePage extends StatefulWidget {
  @override
  DetailCategorieState createState() => DetailCategorieState();
}

class DetailCategorieState extends State<DetailCategoriePage> {
  @override
  Widget build(BuildContext context) {
    Map arguments;
    String nom;
    int id;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print(arguments['nom']);
      nom = arguments['nom'];
      print(arguments['id']);
      id = arguments['id'];
    }
    return DetailCategorie(nom: nom, id: id);
  }
}

class DetailCategorie extends StatefulWidget {
  final String nom;
  final int id;
  DetailCategorie({Key key, this.id, this.nom});

  @override
  _DetailCategoriePageState createState() => _DetailCategoriePageState();
}

class _DetailCategoriePageState extends State<DetailCategorie> {
  int _selectedIndex = 1;
  Widget appBar;
  bool isFavoris = false;
  bool isAbonne;
  Future<Podcasts> podcast;
  bool show = false;
  var listPodcast = new List<PodcastsData>();
  int currentPage = 1;
  int maxPage;
  ScrollController _scrollController;
  String language;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (index == 0) {
        appBar = AppBarHeader(
            name: "Message",
            buttonback: false,
            actionTalk: false,
            actionmessage: true);
      }
      if (index == 1) {
        appBar = AppBarHeader(
            name: widget.nom,
            buttonback: true,
            actionTalk: false,
            actionmessage: false);
      }

      if (index == 2) {
        appBar = null;
      }
    });
  }

  getdetailCategorie(String token, int id, int page) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    // here you "register" to get "notifications" when the getEmail method is done.
    podcast = ClientApi.getPodcastByCategories(token, id, page);
    podcast
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                print(value.data);
                listPodcast = value.data;
                maxPage = value.pagination.pages;
                show = true;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  @override
  void initState() {
    super.initState();
    getToken().then((cleapi) {
      getdetailCategorie(cleapi, widget.id, currentPage);
    });
    Devicelocale.currentLocale.then((value) => {
          setState(() {
            language = value.split(Platform.isAndroid ? '_' : '-')[0];
          })
        });
    appBar = AppBarHeader(
        name: widget.nom,
        buttonback: true,
        actionTalk: false,
        actionmessage: false);
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          currentPage < maxPage) {
        _getMoreData();
      }
    });
  }

  _getMoreData() {
    currentPage++;
    getToken().then((cleapi) {
      podcast =
          ClientApi.getPodcastByCategories(cleapi, widget.id, currentPage);
      podcast
          .then((value) => {
                EasyLoading.dismiss(),
                setState(() {
                  print(value.data);
                  listPodcast = listPodcast + value.data;
                })
              })
          .catchError((error) =>
              {EasyLoading.dismiss(), print("erreur : " + error.toString())});
    });
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      DiscussionPage(),
      Scaffold(
        body: Container(
            color: Color(0xfff8f9f9),
            child: Column(children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                      child: Text(
                          (language == "fr")
                              ? (widget.nom == 'Histoire' ||
                                      widget.nom ==
                                          'Entreprise et Technologie' ||
                                      widget.nom == 'Éducation' ||
                                      widget.nom == 'Humour')
                                  ? "Talks d'${widget.nom}"
                                  : "Talks de ${widget.nom}"
                              : widget.nom + " Talks",
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: Color(0xff3f3c3c),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0.4166666666666666,
                          )),
                      padding: EdgeInsets.fromLTRB(9, 15, 7, 15))),
              Expanded(
                  child: (listPodcast.isEmpty && show)
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset(("assets/icon/mic-3-barr.svg")),
                            Text(
                                AppLocalizations.of(context)
                                    .translate('empty_talks'),
                                style: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Color(0xff9b9b9b),
                                  fontSize: 21,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ))
                          ],
                        )
                      : ListView.builder(
                          itemCount: currentPage == maxPage
                              ? listPodcast.length
                              : listPodcast.length + 1,
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          controller: _scrollController,
                          itemBuilder: (BuildContext ctxt, int index) {
                            if (listPodcast.isNotEmpty) {
                              return (index >= listPodcast.length)
                                  ? Center(child: CupertinoActivityIndicator())
                                  : GestureDetector(
                                      onTap: () {
                                        Navigator.pushNamed(
                                            context, "/detailTalks",
                                            arguments: {
                                              'id': listPodcast[index].id,
                                              'titre': listPodcast[index].nom,
                                            }).then((value) => {
                                              Navigator.of(context)
                                                  .pushReplacementNamed(
                                                      '/detailCategorie',
                                                      arguments: {
                                                    'selectedindex': 1,
                                                    'id': widget.id,
                                                    'nom': widget.nom
                                                  }),
                                            });
                                      },
                                      child: PodcastWidget(
                                        listPodcast[index],
                                        reset: () =>
                                            reset(listPodcast[index].id),
                                        refreshPodcast: () {
                                          Navigator.of(context)
                                              .pushReplacementNamed(
                                                  '/detailCategorie',
                                                  arguments: {
                                                'selectedindex': 1,
                                                'id': widget.id,
                                                'nom': widget.nom
                                              });
                                        },
                                      ));
                            }
                          })),
            ])),
      ),
      MyWorldPage(),
    ];
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        appBar: appBar,
        body: Container(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: Footer(_selectedIndex, _onItemTapped));
  }

  reset(id) {
    for (int i = 0; i < listPodcast.length; i++) {
      if (listPodcast[i].playerState == PlayerState.playing)
        setState(() {
          listPodcast[i].playerState = PlayerState.stopped;
        });
    }
  }
}

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("apikey");
}
