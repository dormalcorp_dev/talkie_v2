import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/data.dart';

class ResetPassword2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ResetPassword2Page();
  }
}

class ResetPassword2Page extends StatefulWidget {
  @override
  _ResetPassword2PageState createState() => _ResetPassword2PageState();
}

class _ResetPassword2PageState extends State<ResetPassword2Page>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  FocusNode _focusNode = FocusNode();
  TextEditingController codeController = new TextEditingController();

  TextStyle style = TextStyle(
    fontFamily: 'Ubuntu',
    color: Color(0xffffffff),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );
  bool is_checked = false;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final codeField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  is_checked = true;
                });
              } else {
                setState(() {
                  is_checked = false;
                });
              }
            },
            style: style,
            controller: codeController,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('enter_code'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final sendButon = Padding(
        padding: EdgeInsets.fromLTRB(38, 71, 37, 0),
        child: SizedBox(
          width: double.infinity, // match_parent
          height: 45,
          child: RaisedButton(
            elevation: 0,
            color: (is_checked) ? Colors.white : Color(0xff6cc5b8),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            onPressed: () {
              if (is_checked) {
                Navigator.pushNamed(context, "/resetpassword3",
                    arguments: {'code': codeController.text});
              }
            },
            child: Text(AppLocalizations.of(context).translate('next'),
                style: TextStyle(
                  fontFamily: 'Ubuntu',
                  color: Color(0xff25a996),
                  fontSize: 15,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                )),
          ),
        ));

    return Scaffold(
      backgroundColor: Color(0xff25a996),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xff25a996),
      ),
      body: Stack(children: <Widget>[
        SingleChildScrollView(
          child: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onPanDown: (_) {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            behavior: HitTestBehavior.translucent,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 172.0,
                    child: Image.asset(
                      "assets/logo.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.only(top: 0, right: 52, left: 18),
                          child: Text(AppLocalizations.of(context).translate('forgot_your_password'),
                              style: TextStyle(
                                fontFamily: 'Salome',
                                color: Color(0xffffffff),
                                fontSize: 28,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding:
                              EdgeInsets.only(top: 15, right: 52, left: 18),
                          child: Text(AppLocalizations.of(context).translate('enter_code_email'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xffffffff),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  codeField,
                  SizedBox(
                    height: 20.0,
                  ),
                  sendButon,
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
