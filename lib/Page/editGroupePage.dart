import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/creationGroupePage1.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/DetailGroupe.dart';
import 'package:voicechanger/data/network/Model/dataGroupe.dart';

import 'homePage.dart';

class EditGroupePage extends StatefulWidget {
  EditGroupe createState() => EditGroupe();
}

class EditGroupe extends State<EditGroupePage> {
  List<Participant> selectedAmis = new List<Participant>();
  TextEditingController _controller = new TextEditingController();
  Future<DataGroupe> data;
  int id;
  Future<DetailGroupe> detailGroup;
  InfosGroupe infoGroup;

  void onValueChange() {
    setState(() {
      _controller.text;
    });
  }

  File _image;
  String _base64Image;

  @override
  void initState() {
    super.initState();
    _controller.addListener(onValueChange);
    getToken().then((token) {
      getInfosGroupe(token, id);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map arguments;
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

    if (arguments != null) {
      print(arguments['id']);
      id = arguments['id'];
    }

    return Scaffold(
        backgroundColor: Color(0xfff8f9f9),
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
                                            backgroundColor: Color(0xff25a996),
          title: Text(AppLocalizations.of(context).translate('edit_group'),
              style: TextStyle(
                fontFamily: 'Roboto',
                color: Color(0xffffffff),
                fontSize: 17,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              )),
          actions: <Widget>[
            FlatButton(
                child: Text(AppLocalizations.of(context).translate('edit'),
                    style: TextStyle(
                      fontFamily: 'SFCompactText',
                      color: Color(0xffffffff),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    )),
                onPressed: () {
                  getToken().then((token) {
                    editGroupe(token, _controller.text, _base64Image,
                        selectedAmis, id);
                  });
                }),
          ],
        ),
        resizeToAvoidBottomInset: false,
        body: Column(children: [
          if (infoGroup != null)
            StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    final act = CupertinoActionSheet(
                        actions: <Widget>[
                          CupertinoActionSheetAction(
                            child: Text(AppLocalizations.of(context).translate('choose_photo'),
                                style: TextStyle(
                                  fontFamily: 'SFProText',
                                  color: Color(0xff262626),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            onPressed: () {
                              Navigator.of(context, rootNavigator: true).pop();

                              getImage(ImageSource.gallery);
                            },
                          ),
                          CupertinoActionSheetAction(
                            child: Text(AppLocalizations.of(context).translate('take_picture'),
                                style: TextStyle(
                                  fontFamily: 'SFProText',
                                  color: Color(0xff262626),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0,
                                )),
                            onPressed: () {
                              Navigator.of(context, rootNavigator: true).pop();

                              getImage(ImageSource.camera);
                            },
                          )
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          child: Text(AppLocalizations.of(context).translate('cancel'),
                              style: TextStyle(
                                fontFamily: 'SFProText',
                                color: Color(0xff262626),
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              )),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                          },
                        ));
                    showCupertinoModalPopup(
                        context: context,
                        builder: (BuildContext context) => act);
                  },
                  child: Container(
                      margin: EdgeInsets.only(top: 18),
                      width: 100,
                      height: 100,
                      decoration: new BoxDecoration(
                        color: Color(0xa8000000),
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x25000000),
                              offset: Offset(0, 5),
                              blurRadius: 15,
                              spreadRadius: 0)
                        ],
                      ),
                      alignment: Alignment.center,
                      child: (_image != null)
                          ? Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: FileImage(_image))))
                          : new Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  Endpoints.baseUrlImage + infoGroup.photo),
                            )))));
            }),
          if (infoGroup != null)
            Padding(
                padding: EdgeInsets.fromLTRB(8, 35, 7, 0),
                child: TextField(
                  controller: _controller,
                  decoration: InputDecoration(
                    hintText: AppLocalizations.of(context).translate('name_group_cool'),
                    labelText: infoGroup.nom,
                    labelStyle: TextStyle(
                      color: Color(0xff3f3c3c),
                      fontSize: 12,
                      fontFamily: 'Ubuntu',
                    ),
                    suffixIcon: IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () {
                          _controller.clear();
                        }),
                    hintStyle: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xffc6c6c6),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    ),
                    counterText: "${_controller.text.length}/40",
                  ),
                  maxLength: 40,
                )),
          if (infoGroup != null)
            new Container(
                margin: EdgeInsets.fromLTRB(8, 27, 8, 0),
                child: GridView.builder(
                  itemCount: selectedAmis.length + 1,
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    // number of items per row
                    crossAxisCount: 3,
                    // vertical spacing between the items
                    mainAxisSpacing: 30.0,
                    // horizontal spacing between the items
                    crossAxisSpacing: 15.0,
                    childAspectRatio: 1.0,
                  ),
                  itemBuilder: (BuildContext context, int i) {
                    if (i == selectedAmis.length)
                      return GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () async {
                            int id = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    settings: RouteSettings(arguments: {
                                      'infoGroup': infoGroup,
                                    }),
                                    builder: (_) => CreationGroupePage1()));
                            getToken().then((token) {
                              getInfosGroupe(token, id);
                            });
                          },
                          child: Column(children: [
                            Container(
                                width: 52,
                                height: 52,
                                decoration: new BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x33000000),
                                          offset: Offset(0, 0),
                                          blurRadius: 5,
                                          spreadRadius: 0)
                                    ],
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(8)),
                                child: DottedBorder(
                                  radius: Radius.circular(8),
                                  dashPattern: [4],
                                  strokeWidth: 1,
                                  strokeCap: StrokeCap.round,
                                  borderType: BorderType.RRect,
                                  color: Color(0xffc6c6c6),
                                  child: Padding(
                                      padding: EdgeInsets.all(10),
                                      child: SvgPicture.asset(
                                          ("assets/icon/add-user.svg"))),
                                )),
                            SizedBox(
                              height: 4,
                            ),
                            Text(AppLocalizations.of(context).translate('add'),
                                style: TextStyle(
                                  fontFamily: 'Ubuntu',
                                  color: Color(0xff3f3c3c),
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 0.2708333333333333,
                                ))
                          ]));
                    else
                      return UserWidget(selectedAmis[i]);
                  },
                ))
        ]));
  }

  Future getImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);
    if (image != null) {
      List<int> imageBytes = image.readAsBytesSync();
      print(imageBytes);
      String base64Image = base64Encode(imageBytes);
      print(base64Image);

      setState(() {
        _image = image;
        _base64Image = base64Image;
      });
    }
  }

  getInfosGroupe(String token, int id) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    detailGroup = ClientApi.getInfoGroup(token, id);

    detailGroup
        .then((value) => {
              EasyLoading.dismiss(),
              setState(() {
                infoGroup = value.data;
                selectedAmis = value.data.participant;
              })
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  editGroupe(String token, String name, String photo, var list, int id) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    data = ClientApi.editgroupe(token, name, photo, list, id);
    data
        .then((value) => {
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(AppLocalizations.of(context).translate('group_edit')),
              _controller.clear(),
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false),
            })
        .catchError((error) =>
            {EasyLoading.dismiss(), print("erreur : " + error.toString())});
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  UserWidget(Participant user) {
    return Column(children: [
      if (user != null)
        Container(
          width: 52,
          height: 52,
          decoration: new BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Color(0x33000000),
                    offset: Offset(0, 0),
                    blurRadius: 5,
                    spreadRadius: 0)
              ],
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(Endpoints.baseUrlImage + user.photo),
              ),
              color: Color(0xffffffff),
              borderRadius: BorderRadius.circular(8)),
        ),
      SizedBox(
        height: 4,
      ),
      if (user != null)
        Text(user.nom,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Ubuntu',
              color: Color(0xff3f3c3c),
              fontSize: 13,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: 0.2708333333333333,
            )),
    ]);
  }
}
