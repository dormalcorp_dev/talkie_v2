import 'dart:async';
import 'dart:io';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:callkeep/callkeep.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/Page/AppBarHeader.dart';
import 'package:voicechanger/Page/Footer.dart';
import 'package:voicechanger/Page/myWorldPage.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import 'TalksPage.dart';
import 'discussionPage.dart';
import 'package:uuid/uuid.dart';
import 'package:firebase_core/firebase_core.dart';

final FlutterCallkeep _callKeep = FlutterCallkeep();
bool _callKeepInited = false;
RtcEngine _engine;

class Home extends StatefulWidget {
  @override
  HomePage createState() => HomePage();
}

class Calll {
  Calll(this.number);
  String number;
  String callID;
  String callerId;
  bool held = false;
  bool muted = false;
}

class HomePage extends State<Home> {
  String appBarTitle = "Discussions";
  static int i = 0;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  int _selectedIndex = 0;
  String pushkitToken;
  Widget appBar = AppBarHeader(
      name: "Discussions",
      buttonback: false,
      actionTalk: false,
      actionmessage: true);
  String newUUID() => Uuid().v4();
  Map<String, Calll> calls = {};
  String user_id;
  final FlutterCallkeep _callKeep = FlutterCallkeep();
  Future<Data> data;
  String language;
  var calludid;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (index == 0) {
        appBar = AppBarHeader(
            name: "Discussions",
            buttonback: false,
            actionTalk: false,
            actionmessage: true);
      }
      if (index == 1) {
        appBar = AppBarHeader(
            name: "Talk",
            buttonback: false,
            actionTalk: true,
            actionmessage: false);
      }
      if (index == 2) {
        appBar = null;
      }
    });
  }



  @override
  initState() {
    Future.delayed(Duration.zero, () {
      setState(() {
        Map arguments;
        if (ModalRoute.of(context) != null) {
          arguments = ModalRoute.of(context).settings.arguments as Map;
        }

        if (arguments != null) {
          print(arguments['selectedindex']);
          _selectedIndex = arguments['selectedindex'];
          appBar = null;
          if (_selectedIndex == 1) {
            appBar = AppBarHeader(
                name: "Talk",
                buttonback: false,
                actionTalk: true,
                actionmessage: false);
          }
        }
      });
    });
    super.initState();
    Devicelocale.currentLocale.then((value) => {
          setState(() {
            language = value.split(Platform.isAndroid ? '_' : '-')[0];
          })
        });
 
    getToken().then((id) {
      getUserID(id);
    });
    _callKeep.on(CallKeepDidDisplayIncomingCall(), didDisplayIncomingCall);
    _callKeep.on(CallKeepPerformAnswerCallAction(), answerCall);
    _callKeep.on(CallKeepDidPerformDTMFAction(), didPerformDTMFAction);
    _callKeep.on(
        CallKeepDidReceiveStartCallAction(), didReceiveStartCallAction);
    _callKeep.on(CallKeepDidToggleHoldAction(), didToggleHoldCallAction);
    _callKeep.on(
        CallKeepDidPerformSetMutedCallAction(), didPerformSetMutedCallAction);
    _callKeep.on(CallKeepPerformEndCallAction(), endCall);
    _callKeep.on(CallKeepPushKitToken(), onPushKitToken);

    _callKeep.setup(context,<String, dynamic>{
      'ios': {
        'appName': 'talkie',
      },
      'android': {
        'alertTitle': 'Permissions required',
        'alertDescription':
            'This application needs to access your phone accounts',
        'cancelButton': 'Cancel',
        'okButton': 'ok',
      },
    });
  

    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    _requestIOSPermission();
    FirebaseMessaging.onBackgroundMessage(myBackgroundMessageHandler);

    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);
if(i%2==0) {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      print("onMessage: ${message.data}");
             

      RemoteNotification notification = message.notification;
      if (message.data['type'] == "Call") {
        print(message.data['caller_id']);
        final dynamic data = message.data;
        var number = data['chanel_id'] as String;
        var caller = data['caller_name'] as String;
        var caller_id = data['caller_id'] as String;
        var callId = data['call_id'] as String;
        
        await displayIncomingCall(number, caller);

        /*   Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PickupScreen(call: Call(
                                callerId: message['data']['caller_id'].toString(),
                                callerName: message['data']['caller_name'],
                                callerPic: message['data']['caller_photo'],
                                 channelId:   message['data']['chanel_id'].toString()                          
                              )),
                              ),
                            );*/
      } else if (message.data['type'] == "End Call") {
        final dynamic data = message.data;

        var callerId = data['caller_id'] as String;

        print("Endd calll");
        
      
       if (int.parse(callerId) == int.parse(user_id)) {
         if(Navigator.of(context).canPop()){
          Navigator.pop(context);          
          }
        } else {
            _engine?.leaveChannel();
    _engine?.destroy();
 removeCall(calludid);
   _callKeep?.endCall(calludid);
          print("exit from call");
        }
      } else {
        showNotification(notification.title, notification.body);
      }
    });
    



    
    }
     i++;
  }

  @override
  void dispose() {
    _engine?.leaveChannel();
    _engine?.destroy();
    super.dispose();
  }

  Future selectNotification(String payload) async {
    if (payload != null) print(payload);
    /*showDialog(
      context: context,
      builder: (_) {
        return new AlertDialog(
          title: Text("PayLoad"),
          content: Text("Payload : $payload"),
        );
      },
    );*/
  }

  void showNotification(String title, String body) async {
    await _demoNotification(title, body);
  }

  Future<void> _demoNotification(String title, String body) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'channel_ID', 'channel name', 'channel description',
        importance: Importance.max,
        playSound: true,
        showProgress: true,
        priority: Priority.high,
        ticker: 'test ticker');

    var iOSChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics, iOS: iOSChannelSpecifics);
    await flutterLocalNotificationsPlugin
        .show(0, title, body, platformChannelSpecifics, payload: 'test');
  }


   
 

 final _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      DiscussionPage(),
      Talks(),
      MyWorldPage()
    ];

    return Scaffold(
              key: _scaffoldKey,

        appBar: appBar,
        body: Container(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: Footer(_selectedIndex, _onItemTapped));
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  Future<int> getCallId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("call_id");
  }

  Future<String> getChannelId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("number");
  }

  Future<String> getTokenFirebase() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("token");
  }

  getUserID(String token) {
    // here you "register" to get "notifications" when the getEmail method is done.
    data = ClientApi.getUserID(token);
    data
        .then((value) => {
              setState(() {
                user_id = value.data;
                pushUserID(int.parse(value.data));
              })
            })
        .catchError((error) => {
              if (error == 401) {logoutUser()},
              print("erreur : " + error.toString())
            });
  }
 Future<String> pushUserID(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('id', id);
  }
  Future<void> logoutUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear().then((onValue) => {
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/login', (Route<dynamic> route) => false)
        });
  }

  Future<void> onPushKitToken(CallKeepPushKitToken event) async {
    print('[onPushKitToken] token => ${event.token}');
        String tokenFcm = await FirebaseMessaging.instance.getToken();

    getToken().then((token) => {updateTokenFirebase(token, tokenFcm,event.token)});
  }





  updateTokenFirebase(String token, String token_firebase,String pushkitToken) async {
    data = ClientApi.updateTokenFCM(token, token_firebase,pushkitToken);
    data
        .then((value) => {
              print(value),
              setToken(token_firebase).then((token_firebase) => {})
            })
        .catchError((error) => {print("erreur : " + error.toString())});
  }

  Future<String> setToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  _requestIOSPermission() async {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    
  }

  void removeCall(String callUUID) {
    setState(() {
      calls.remove(callUUID);
    });
  }

  void setCallHeld(String callUUID, bool held) {
    setState(() {
      calls[callUUID].held = held;
    });
  }

  void setCallMuted(String callUUID, bool muted) {
    setState(() {
      calls[callUUID].muted = muted;
    });
  }

  Future<void> answerCall(CallKeepPerformAnswerCallAction event) async {
    final String callUUID = event.callUUID;
    final String number = calls[callUUID].number ;
    final String callID = calls[callUUID].callID ;

    

    print('[answerCall] $callUUID, number: $number ,callID: $callID');

    _callKeep.startCall(callUUID, null, number);
    Timer(const Duration(seconds: 1), () {
      print('[setCurrentCallActivee] $callUUID, number: $number');
      print(number);
      _callKeep.setCurrentCallActive(callUUID);
      initializeAgora(number);
    acceptCall(callID);
    });
  }
Future<void> _initAgoraRtcEngine(number) async {
  _engine = await RtcEngine.create(Endpoints.APP_ID);
  await _engine.setParameters('{"che.audio.opensl":true}');
  await _engine.disableVideo();
  await _engine.setChannelProfile(ChannelProfile.Communication);
  await _engine.joinChannel(null, number, null, 0);
}

Future<void> initializeAgora(number) async {
  await _initAgoraRtcEngine(number);
  _addAgoraEventHandlers();
  await _engine.enableWebSdkInteroperability(true);
  VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
  configuration.dimensions = VideoDimensions(1920, 1080);
  await _engine.setVideoEncoderConfiguration(configuration);
}

void _addAgoraEventHandlers() {
  _engine.setEventHandler(RtcEngineEventHandler(
      error: (code) {},
      joinChannelSuccess: (channel, uid, elapsed) {},
      leaveChannel: (stats) {
      },
      userJoined: (uid, elapsed) {},
      userOffline: (uid, elapsed) {
        print(calludid);
               _engine?.leaveChannel();
    _engine?.destroy();
 removeCall(calludid);
   _callKeep?.endCall(calludid);

      },
      
      firstRemoteVideoFrame: (uid, width, height, elapsed) {}));
}

  Future<void> endCall(CallKeepPerformEndCallAction event) async {
     if( calls.isNotEmpty){
    print('endCall: ${event.callUUID}');
    final String number = calls[event.callUUID].number ;
    final String callerId = calls[event.callUUID].callerId ;
    final String callID = calls[event.callUUID].callID ;
    print('callerID $callerId, number: $number ,callID: $callID');
   
    endCallApi(int.parse(callerId), number, int.parse(callID));

   _engine?.leaveChannel();
    _engine?.destroy();
 removeCall(calludid);
}
  }

  Future<void> didPerformDTMFAction(CallKeepDidPerformDTMFAction event) async {
    print('[didPerformDTMFAction] ${event.callUUID}, digits: ${event.digits}');
  }

  Future<void> didReceiveStartCallAction(
      CallKeepDidReceiveStartCallAction event) async {
    if (event.handle == null) {
      // @TODO: sometime we receive `didReceiveStartCallAction` with handle` undefined`
      print("undifinedd");
      return;
    }
    final String callUUID = event.callUUID ?? newUUID();
    setState(() {
      calls[callUUID] = Calll(event.handle);
    });
    print('[didReceiveStartCallAction] $callUUID, number: ${event.handle}');

    _callKeep.startCall(callUUID, event.handle, event.handle);

    Timer(const Duration(seconds: 1), () {
      print('[setCurrentCallActive] $callUUID, number: ${event.handle}');
      _callKeep.setCurrentCallActive(callUUID);
    });
  }

  Future<void> didPerformSetMutedCallAction(
      CallKeepDidPerformSetMutedCallAction event) async {
    final String number = calls[event.callUUID].number;
    print(
        '[didPerformSetMutedCallAction] ${event.callUUID}, number: $number (${event.muted})');

    setCallMuted(event.callUUID, event.muted);
  }

  Future<void> didToggleHoldCallAction(
      CallKeepDidToggleHoldAction event) async {
    final String number = calls[event.callUUID].number;
    print(
        '[didToggleHoldCallAction] ${event.callUUID}, number: $number (${event.hold})');

    setCallHeld(event.callUUID, event.hold);
  }

  Future<void> hangup(String callUUID) async {
    _callKeep.endCall(callUUID);
    removeCall(callUUID);
  }

  Future<void> setOnHold(String callUUID, bool held) async {
    _callKeep.setOnHold(callUUID, held);
    final String handle = calls[callUUID].number;
    print('[setOnHold: $held] $callUUID, number: $handle');
    setCallHeld(callUUID, held);
  }

  Future<void> setMutedCall(String callUUID, bool muted) async {
    _callKeep.setMutedCall(callUUID, muted);
    final String handle = calls[callUUID].number;
    print('[setMutedCall: $muted] $callUUID, number: $handle');
    setCallMuted(callUUID, muted);
  }

  Future<void> updateDisplay(String callUUID) async {
    final String number = calls[callUUID].number;
    // Workaround because Android doesn't display well displayName, se we have to switch ...
    if (isIOS) {
      _callKeep.updateDisplay(callUUID,
          displayName: 'New Name', handle: number);
    } else {
      _callKeep.updateDisplay(callUUID,
          displayName: number, handle: 'New Name');
    }

    print('[updateDisplay: $number] $callUUID');
  }

  Future<void> displayIncomingCallDelayed(
      String number, String caller, String caller_id, String callId) async {
    Timer(const Duration(seconds: 3), () {
      displayIncomingCall(number, caller);
    });
  }

  Future<void> displayIncomingCall(String number, String caller,) async {
    final String callUUID = newUUID();
    setState(() {
     calludid=callUUID;
      calls[callUUID] = Calll(number);
    });
    print('Display incoming call now');
    final bool hasPhoneAccount = await _callKeep.hasPhoneAccount();
    if (!hasPhoneAccount) {
      
      await _callKeep.hasDefaultPhoneAccount(context, <String, dynamic>{
          'ios': {
          'appName': 'Talkie',
        },
        'android': {
          'alertTitle': 'Permissions required',
          'alertDescription':
              'This application needs to access your phone accounts',
          'cancelButton': 'Cancel',
          'okButton': 'ok',
        },
      });
    }

    print('[displayIncomingCall] $callUUID number: $number' "");
    if (Platform.isAndroid) {
      _callKeep.displayIncomingCall(callUUID, caller,
          handleType: 'number', hasVideo: false);
    } else if (Platform.isIOS) {
    _callKeep.displayIncomingCall(callUUID, number,
        handleType: 'number', hasVideo: false,localizedCallerName: caller);

    }
  }

  void didDisplayIncomingCall(CallKeepDidDisplayIncomingCall event) {
  var callUUID = event.callUUID;
    var number = event.handle;
    var call_id = event.call_id;
    print('[displayIncomingCall] $callUUID number: $number,call_id: $call_id');
    setState(() {
      calludid=callUUID;
      calls[callUUID] = Calll(number);
      calls[callUUID].callID = event.call_id;
      calls[callUUID].callerId = event.callerId;
    });
  }

  acceptCall(call_id) {
    getToken().then((token) => {
          data = ClientApi.acceptCall(token, int.parse(call_id)),
          data
              .then((value) => {
                    print("acccept"),
                  })
              .catchError((error) => {
                    Fluttertoast.showToast(
                        msg: error.toString(),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.CENTER,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0),
                    print("erreur : " + error.toString())
                  }),
        });
  }

  endCallApi(callerId, channelID, call_id) {
    getToken().then((token) => {
          data =
              ClientApi.endCall(token, callerId, channelID, call_id),
          data
              .then((value) => {
                    print("enddd"),
                  })
              .catchError((error) => {
                    Fluttertoast.showToast(
                        msg: error.toString(),
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.CENTER,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0),
                    print("erreur : " + error.toString())
                  }),
        });
  }
  



  

 
}

Future<dynamic> myBackgroundMessageHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
    print('backgroundMessage: message => ${message.toString()}');

  if (message.data['type'] == "Call") {

    final dynamic data = message.data;
    var number = data['chanel_id'] as String;
    var callername = data['caller_name'] as String;
    var callerId = data['caller_id'] as String;
    var callId = data['call_id'] as String;

    print("aaaaaaa");
    print(callername);
    final callUUID = Uuid().v4();

    _callKeep.on(CallKeepPerformAnswerCallAction(),
        (CallKeepPerformAnswerCallAction event) {
      print(
          'backgroundMessage: CallKeepPerformAnswerCallAction ${event.callUUID}');
      _callKeep.startCall(callUUID, null, number);
      print("abc :" + event.callUUID);
      Timer(const Duration(seconds: 1), () {
        print('[setCurrentCallActive] $callUUID, number: $number');
        _callKeep.setCurrentCallActive(callUUID);
      });
      //_callKeep.endCall(event.callUUID);
    });
    _callKeep.on(CallKeepPerformEndCallAction(),
        (CallKeepPerformEndCallAction event) {
      print(
          'backgroundMessage: CallKeepPerformEndCallAction ${event.callUUID}');
    });


    print('backgroundMessage: displayIncomingCall ($number)');
  if (!_callKeepInited) {
    _callKeep.setup(null, <String, dynamic>{
      'ios': {
        'appName': 'CallKeepDemo',
      },
      'android': {
        'alertTitle': 'Permissions required',
        'alertDescription':
            'This application needs to access your phone accounts',
        'cancelButton': 'Cancel',
        'okButton': 'ok',
        'foregroundService': {
          'channelId': 'com.company.my',
          'channelName': 'Foreground service for my app',
          'notificationTitle': 'My app is running on background',
          'notificationIcon': 'Path to the resource icon of the notification',
        },
      },
    });
    _callKeepInited = true;
  }

    _callKeep.displayIncomingCall(callUUID, callername,
        handleType: '', hasVideo: false);
    _callKeep.backToForeground();
    final prefs = await SharedPreferences.getInstance();
    // SharedPreferences could have been modified in the UI isolate, so we have to fetch it from the host platform.

    // await prefs.getOrSetWhateverYouNeed();

    /*

  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    print('notification => ${notification.toString()}');
  }

  // Or do other work.
  */

  } else if (message.data['type'] == "End Call") {
    print("enndcall");

    _callKeep.endAllCalls();
  }
  return null;
}

