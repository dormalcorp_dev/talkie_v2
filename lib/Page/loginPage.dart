import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';

import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/login.dart';

import 'package:flutter_easyloading/flutter_easyloading.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  FocusNode _focusNode = FocusNode();
  bool is_checked1 = false;
  bool is_checked2 = false;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();

    super.dispose();
  }

  Future<String> _loginSuccess(String apikey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('apikey', "Bearer " + apikey);
  }

  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  Future<Login> loginapi;
  TextStyle style = TextStyle(
    fontFamily: 'Ubuntu',
    color: Color(0xffffffff),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );

  @override
  Widget build(BuildContext context) {
    final emailField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  is_checked1 = true;
                });
              } else {
                setState(() {
                  is_checked1 = false;
                });
              }
            },
            style: style,
            controller: emailController,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('identifier'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final passwordField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextField(
            onChanged: (value) {
              if (value.isNotEmpty) {
                setState(() {
                  is_checked2 = true;
                });
              } else {
                setState(() {
                  is_checked2 = false;
                });
              }
            },
            style: style,
            controller: passwordController,
            obscureText: true,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('password'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final signupButon = Padding(
        padding: EdgeInsets.fromLTRB(38, 71, 37, 0),
        child: SizedBox(
            width: double.infinity, // match_parent
            height: 45,
            child: RaisedButton(
                elevation: 0,
                color: (is_checked1 && is_checked2)
                    ? Colors.white
                    : Color(0xff6cc5b8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
                onPressed: () {
                  if (is_checked1 && is_checked2) {
                    EasyLoading.instance
                      ..loadingStyle = EasyLoadingStyle.custom
                      ..backgroundColor = Colors.white
                      ..progressColor = Colors.white
                      ..indicatorType = EasyLoadingIndicatorType.wave
                      ..indicatorColor = Color(0xff3bc0ae)
                      ..textColor = Color(0xff3bc0ae)
                    
                      ..userInteractions = false
                      ..maskType = EasyLoadingMaskType.black;
                    EasyLoading.show(
                      status: AppLocalizations.of(context).translate('loading'),
                    );
                    loginapi = ClientApi.loginApi(
                        emailController.text, passwordController.text);
                    loginapi
                        .then((value) => {
                              _loginSuccess(value.token).then((onValue) => {
                                    EasyLoading.dismiss(),
                                    Navigator.of(context)
                                        .pushNamedAndRemoveUntil('/home',
                                            (Route<dynamic> route) => false),
                                  }),
                            })
                        .catchError((error) => {
                             /* emailController.addError('Wrong ID.'),
                              passwordController.addError('Wrong Password'),*/
                              
                              print(error),
                              EasyLoading.dismiss(),
                              EasyLoading.instance.loadingStyle =
                                  EasyLoadingStyle.custom,
                              EasyLoading.instance.backgroundColor = Colors.red,
                              EasyLoading.instance.indicatorColor =
                                  Colors.white,
                              EasyLoading.instance.progressColor = Colors.white,
                              EasyLoading.instance.textColor = Colors.white,
                              EasyLoading.showError(AppLocalizations.of(context).translate('error_connection'))
                            });
                  }
                },
                child: Text(AppLocalizations.of(context).translate('login'),
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff25a996),
                      fontSize: 15,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )))));

    final forgotButon = new Align(
        alignment: Alignment.topLeft,
        child: Padding(
            padding: EdgeInsets.fromLTRB(7, 0, 0, 0),
            child: TextButton(
                child: new Text(AppLocalizations.of(context).translate('password_forgot'),
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xffffffff),
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    )),
                onPressed: () {
                  Navigator.pushNamed(context, "/resetpassword1");
                })));


    final activateButon = new Align(
        alignment: Alignment.topLeft,
        child: Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 7, 0),
            child: FlatButton(
                child: new Text(AppLocalizations.of(context).translate('activate_account'),
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xffffffff),
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    )),
                onPressed: () {
                  Navigator.pushNamed(context, "/activate1");
                })));            

    final login = Padding(
        padding: EdgeInsets.fromLTRB(0, 15, 0, 60),
        child: Text.rich(
          TextSpan(
            text: AppLocalizations.of(context).translate('not_member'),
            style: TextStyle(
              fontFamily: 'Ubuntu',
              color: Color(0xffffffff),
              fontSize: 13,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
            ),
            children: <TextSpan>[
              TextSpan(
                  text: AppLocalizations.of(context).translate('register_login'),
                  recognizer: new TapGestureRecognizer()
                    ..onTap =
                        () => Navigator.pushNamed(context, "/inscription"),
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    color: Color(0xffffffff),
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    decoration: TextDecoration.underline,
                  )),
              // can add more TextSpans here...
            ],
          ),
          overflow: TextOverflow.visible,
        ));

    return Scaffold(
      backgroundColor: Color(0xff25a996),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xff25a996),
      ),
      body: Stack(children: <Widget>[
        SingleChildScrollView(
          child: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onPanDown: (_) {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            behavior: HitTestBehavior.translucent,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 172.0,
                    child: Image.asset(
                      "assets/logo.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.only(top: 0, right: 52, left: 18),
                          child: Text(AppLocalizations.of(context).translate('login'),
                              style: TextStyle(
                                fontFamily: 'Salome',
                                color: Color(0xffffffff),
                                fontSize: 28,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  emailField,
                  passwordField,
                    Row(
          // first row with 3 column
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(child:
                  forgotButon),
                  Expanded(child:
                  activateButon),
          ],
        ),
                  signupButon,
                  login
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
