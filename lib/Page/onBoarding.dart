import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:flutter_svg/svg.dart';
import 'package:voicechanger/app_localizations.dart';

class OnBoardingPage extends StatefulWidget {
  // receive data from the FirstScreen as a parameter

  @override
  OnBoardingPageState createState() => OnBoardingPageState();
}

class OnBoardingPageState extends State<OnBoardingPage> {
  PageController _pageController = PageController();
  double currentPage = 0;
  static const length = 4;
  final pageIndexNotifier = ValueNotifier<int>(0);

  @override
  void initState() {
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page;
      });
    });

    super.initState();

    /// Schedule a callback for the end of this frame
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return Scaffold(
        body: Stack(
          children: <Widget>[
            PageView.builder(
              onPageChanged: (index) => pageIndexNotifier.value = index,
              itemCount: length,
              itemBuilder: (context, index) {
                return Container(
                  child: SingleChildScrollView(
                    child: Column(children: <Widget>[
                      SizedBox(
                        height: 172.0,
                        child: Image.asset(
                          "assets/logo.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                      if (index == 0)
                        SvgPicture.asset("assets/icon/mic-3.svg", height: 137),
                      if (index == 0)
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 79, right: 37, left: 37),
                                child: Text(AppLocalizations.of(context).translate('welcome'),
                                    style: TextStyle(
                                      fontFamily: 'Salome',
                                      color: Color(0xffffffff),
                                      fontSize: 40,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      if (index == 0)
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 15, right: 38, left: 36),
                                child: Text(
                                   AppLocalizations.of(context).translate('welcome_text'),
                                    style: TextStyle(
                                      fontFamily: 'Ubuntu',
                                      color: Color(0xffffffff),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      if (index == 1)
                        SvgPicture.asset(
                          "assets/icon/bubble.svg",
                          height: 127,
                          color: Colors.white,
                        ),
                      if (index == 1)
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 79, right: 37, left: 37),
                                child: Text("Discussions",
                                    style: TextStyle(
                                      fontFamily: 'Salome',
                                      color: Color(0xffffffff),
                                      fontSize: 40,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      if (index == 1)
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 15, right: 38, left: 36),
                                child: Text(
                                    AppLocalizations.of(context).translate('discussions_text'),
                                    style: TextStyle(
                                      fontFamily: 'Ubuntu',
                                      color: Color(0xffffffff),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      if (index == 2)
                        SvgPicture.asset(
                          "assets/icon/mic2.svg",
                          height: 138,
                          color: Colors.white,
                        ),
                      if (index == 2)
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 79, right: 37, left: 37),
                                child: Text("Talks",
                                    style: TextStyle(
                                      fontFamily: 'Salome',
                                      color: Color(0xffffffff),
                                      fontSize: 40,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      if (index == 2)
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 15, right: 38, left: 36),
                                child: Text(
                                    AppLocalizations.of(context).translate('talk_text'),
                                    style: TextStyle(
                                      fontFamily: 'Ubuntu',
                                      color: Color(0xffffffff),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      if (index == 3)
                        SvgPicture.asset(
                          "assets/icon/mic-3-filtre.svg",
                          height: 138,
                          color: Colors.white,
                        ),
                      if (index == 3)
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 79, right: 37, left: 37),
                                child: Text( AppLocalizations.of(context).translate('voice_filters'),
                                    style: TextStyle(
                                      fontFamily: 'Salome',
                                      color: Color(0xffffffff),
                                      fontSize: 40,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      if (index == 3)
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 15, right: 38, left: 36),
                                child: Text(
                                    AppLocalizations.of(context).translate('voice_filters_text'),
                                    style: TextStyle(
                                      fontFamily: 'Ubuntu',
                                      color: Color(0xffffffff),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )))),
                      SizedBox(height: 40),
                      _buildExample1(),
                      Padding(
                          padding: EdgeInsets.fromLTRB(38, 15, 37, 0),
                          child: SizedBox(
                              width: double.infinity, // match_parent
                              height: 45,
                              child: RaisedButton(
                                  elevation: 0,
                                  color: Color(0xff25a996),
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamed(context, "/login");
                                  },
                                  child: Text(AppLocalizations.of(context).translate('login'),
                                      style: TextStyle(
                                        fontFamily: 'Ubuntu',
                                        color: Color(0xffffffff),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      ))))),
                      Padding(
                          padding: EdgeInsets.fromLTRB(38, 15, 37, 0),
                          child: SizedBox(
                              width: double.infinity, // match_parent
                              height: 45,
                              child: RaisedButton(
                                  elevation: 0,
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, "/inscription");
                                  },
                                  child: Text(AppLocalizations.of(context).translate('register_now'),
                                      style: TextStyle(
                                        fontFamily: 'Ubuntu',
                                        color: Color(0xff25a996),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      ))))),
                    ]),
                  ),
                  decoration: BoxDecoration(color: Color(0xff25a996)),
                );
              },
            ),
          ],
        ),
      
     
    );
  }

  PageViewIndicator _buildExample1() {
    return PageViewIndicator(
      pageIndexNotifier: pageIndexNotifier,
      length: length,
      normalBuilder: (animationController, index) => Circle(
        size: 8.0,
        color: Color(0x54ffffff),
      ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController,
          curve: Curves.ease,
        ),
        child: Circle(size: 8.0, color: Color(0xffffffff)),
      ),
    );
  }
}
