import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';



class PdfPage extends StatefulWidget {
  Pdf createState() => Pdf();
}

class Pdf extends State<PdfPage> {
  Map arguments;
  final Completer<PDFViewController> _controller =
      Completer<PDFViewController>();
  int pages = 0;
  int currentPage = 0;
  bool isReady = false;
  String errorMessage = '';
String path;

  @override
  void initState() {
    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
      if (arguments != null) {
      print(arguments['path']);
        path = arguments['path'];
      }
    }

    return Scaffold(
        backgroundColor: Color(0xfff8f9f9),
        appBar: AppBar(        backgroundColor: Color(0xff25a996),
elevation: 0, centerTitle: true, title: (path.contains("cgu")? Text("CGU"): Text("EULA"))),
        resizeToAvoidBottomInset: false,
        body: Stack(
        children: <Widget>[
          PDFView(
            filePath:path,
            enableSwipe: true,
            swipeHorizontal: false,
            autoSpacing: true,
            fitPolicy: FitPolicy.BOTH,
            preventLinkNavigation:
                false, // if set to true the link is handled in flutter
            onRender: (_pages) {
              setState(() {
                pages = _pages;
                isReady = true;
              });
            },
            onError: (error) {
              setState(() {
                errorMessage = error.toString();
              });
              print(error.toString());
            },
            onPageError: (page, error) {
              setState(() {
                errorMessage = '$page: ${error.toString()}';
              });
              print('$page: ${error.toString()}');
            },
            onViewCreated: (PDFViewController pdfViewController) {
              _controller.complete(pdfViewController);
            },
            onLinkHandler: (String uri) {
              print('goto uri: $uri');
            },
            onPageChanged: (int page, int total) {
              print('page change: $page/$total');
              setState(() {
                currentPage = page;
              });
            },
          ),
          errorMessage.isEmpty
              ? !isReady
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : Container()
              : Center(
                  child: Text(errorMessage),
                )
        ],
      ));
}


}