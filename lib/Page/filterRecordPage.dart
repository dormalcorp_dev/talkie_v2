import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:io' as io;
import 'dart:convert';

import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Constant.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import 'package:voicechanger/data/network/Model/filtre.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../player_widget.dart';


class FilterRecordPage extends StatefulWidget {
 final String id_conv;
 final  idReceivers;

  final LocalFileSystem localFileSystem;
  FilterRecordPage({Key key, localFileSystem, this.id_conv, this.idReceivers})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  FilterRecord createState() => FilterRecord();
}

class FilterRecord extends State<FilterRecordPage> {
  WebViewController _controller;
  double viewPortFraction = 0.14;
  double page = 2.0;
  String filechange;
  String base64Image;
  Duration _duration;
  Duration _position;
  bool show_filtre = false;
  FlutterAudioRecorder _recorder;
  Recording _current;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  bool recorder = false;
  String timer;
  File file;
  Timer _timer;
  String duree;
  int start = 60;
  String base64audio;
  int id_filtre;
  Future<Filtre> filtre;
  Future<Data> data;
  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  PageController pageController;
  String url_filtre;
  int currentPage = 3;

  get _durationText =>
      "${_duration?.inMinutes?.remainder(60) ?? ''}:${(_duration?.inSeconds?.remainder(60)) ?? ''}";
  get _positionText =>
      "${_position?.inMinutes?.remainder(60) ?? ''}:${(_position?.inSeconds?.remainder(60)) ?? ''}";

  PlayerState _playerState = PlayerState.stopped;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;
  bool seekDone;
  List<Map<String, String>> listOfCharacters = [
    {'image': "", 'name': "none", "filtre": "3", 'nom': "None"},
    {
      'image': "assets/astronaut.jpg",
      'name': "astronaut",
      'nom': "Astronaute",
      "filtre": "2"
    },
    {
      'image': "assets/church.jpg",
      'name': "church",
      "filtre": "0",
      'nom': "Église"
    },
    {
      'image': "assets/giant-robot.jpg",
      'name': "giantRobot",
      "filtre": "1",
      'nom': "Robot Géant"
    },
    {
      'image': "assets/phaser.jpg",
      'name': "phaser",
      "filtre": "4",
      'nom': "Phaser"
    },
    {
      'image': "assets/melting-clock.jpg",
      'name': "slowWobble",
      "filtre": "5",
      'nom': "SlowWobble"
    },
  ];
  void startTimer(int timerDuration) {
    setState(() {
      start = timerDuration;
    });

    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (start < 1) {
            timer.cancel();
            _stop();
          } else {
            start = start - 1;
          }
        },
      ),
    );
  }

  @override
  void initState() {
    pageController = PageController(
        initialPage: currentPage, viewportFraction: viewPortFraction);

    super.initState();

    _init();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (_playerState == PlayerState.paused ||
              _playerState == PlayerState.playing) {
            await _audioPlayer.release();
          }
          if (status() == 'Start' || status() == 'Pause') {
            EasyLoading.showError(AppLocalizations.of(context).translate('stop_recorder'));
          } else {
            Navigator.pop(context);
          }
          return false;
        },
        child: Container(
            color:Colors.black,
          padding: EdgeInsets.only(top: 54),
          child: Material(

              color: Color(0xff25a996),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0)),
              child: Stack(children: <Widget>[
                Column(children: <Widget>[
                  Align(
                      alignment: Alignment.topLeft,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 24.0,
                          ),
                          onPressed: () async {
                            if (_playerState == PlayerState.paused ||
                                _playerState == PlayerState.playing) {
                              await _audioPlayer.release();
                            }

                            if (status() == 'Start' || status() == 'Pause') {
                              EasyLoading.showError(AppLocalizations.of(context).translate('stop_recorder'));
                            } else {
                              Navigator.pop(context);
                            }
                          })),
                  SizedBox(
                    height: 20,
                  ),
                  Text(AppLocalizations.of(context).translate('record_message'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Salome',
                        color: Color(0xffffffff),
                        fontSize: 30,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.625,
                      )),
                ]),
                Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      if (status() == 'Init')
                        IconButton(
                            iconSize: 132,
                            onPressed: () {
                              _start();
                            },
                            icon: SvgPicture.asset(
                              "assets/icon/group-2-copy-9.svg",
                            )),
                      if (status() == 'Stop' && id_filtre == null)
                        Row(children: [
                          Expanded(
                              child: IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    if (_playerState == PlayerState.playing) {
                                      _pause();
                                    } else {
                                      _play(_current.path,true);
                                    }
                                  },
                                  icon: (_playerState == PlayerState.playing)
                                      ? SvgPicture.asset(
                                          ("assets/icon/stop.svg"))
                                      : SvgPicture.asset(
                                          ("assets/icon/play.svg")))),
                          Expanded(
                              child: SliderTheme(
                                  data: SliderThemeData(
                                      trackHeight: 4,
                                      thumbShape: RoundSliderThumbShape(
                                          enabledThumbRadius: 0)),
                                  child: Slider(
                                    activeColor: Colors.white,
                                    inactiveColor: Color(0xffc6c6c6),
                                    onChanged: (v) {
                                      if (_duration != null) {
                                        final Position =
                                            v * _duration.inMilliseconds;
                                        _audioPlayer.seek(Duration(
                                            milliseconds: Position.round()));
                                      }
                                    },
                                    value: (_position != null &&
                                            _duration != null &&
                                            _position.inMilliseconds > 0 &&
                                            _position.inMilliseconds <
                                                _duration.inMilliseconds)
                                        ? _position.inMilliseconds /
                                            _duration.inMilliseconds
                                        : 0.0,
                                  )),
                              flex: 8),
                          Expanded(
                              child: IconButton(
                                  iconSize: 23,
                                  onPressed: () {
                                    cancelMessage();
                                  },
                                  icon: SvgPicture.asset(
                                      ("assets/icon/carbage.svg")))),
                        ]),
                           if ( url_filtre != null)
                        Row(children: [
                          Expanded(
                              child: IconButton(
                                  iconSize: 30,
                                  onPressed: () {
                                    if (_playerState == PlayerState.playing) {
                                      _pause();
                                    } else {
                                      _play(Endpoints.baseUrlImage+url_filtre,false);
                                    }
                                  },
                                  icon: (_playerState == PlayerState.playing)
                                      ? SvgPicture.asset(
                                          ("assets/icon/stop.svg"))
                                      : SvgPicture.asset(
                                          ("assets/icon/play.svg")))),
                          Expanded(
                              child: SliderTheme(
                                  data: SliderThemeData(
                                      trackHeight: 4,
                                      thumbShape: RoundSliderThumbShape(
                                          enabledThumbRadius: 0)),
                                  child: Slider(
                                    activeColor: Colors.white,
                                    inactiveColor: Color(0xffc6c6c6),
                                    onChanged: (v) {
                                      if (_duration != null) {
                                        final Position =
                                            v * _duration.inMilliseconds;
                                        _audioPlayer.seek(Duration(
                                            milliseconds: Position.round()));
                                      }
                                    },
                                    value: (_position != null &&
                                            _duration != null &&
                                            _position.inMilliseconds > 0 &&
                                            _position.inMilliseconds <
                                                _duration.inMilliseconds)
                                        ? _position.inMilliseconds /
                                            _duration.inMilliseconds
                                        : 0.0,
                                  )),
                              flex: 8),
                          Expanded(
                              child: IconButton(
                                  iconSize: 23,
                                  onPressed: () {
                                    cancelMessage();
                                  },
                                  icon: SvgPicture.asset(
                                      ("assets/icon/carbage.svg")))),
                        ]),
                      if (_playerState == PlayerState.paused ||
                          _playerState == PlayerState.playing)
                        Text("$_positionText s /$_durationText s",
                            style: TextStyle(
                              fontFamily: 'Ubuntu',
                              color: Color(0xffffffff),
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0.4166666666666667,
                            )),
                      if (status() == 'Start')
                        Text("$timer s / 60s",
                            style: TextStyle(
                              fontFamily: 'Ubuntu',
                              color: Color(0xffffffff),
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0.4166666666666667,
                            ))
                    ])),
                if (status() == 'Start')
                  Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: GestureDetector(
                              onTap: () {
                                _stop();
                              },
                              child: Container(
                                  width: 54,
                                  height: 54,
                                  decoration: new BoxDecoration(
                                    border: new Border.all(
                                        color: Colors.white, width: 2),
                                    shape: BoxShape.circle,
                                    color: Color(0xff25a996),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x2b2d2d2d),
                                          offset: Offset(0, 0),
                                          blurRadius: 0,
                                          spreadRadius: 5)
                                    ],
                                  ),
                                  child: Padding(
                                      padding: EdgeInsets.all(14),
                                      child: SvgPicture.asset(
                                          ("assets/icon/stop.svg"))))))),
                /* if(status() =='Stop' && !show_filtre ) 
     Padding(padding: EdgeInsets.only(bottom:20),
child:Align(
  
          alignment: Alignment.bottomCenter,child: 
    GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
       setState(() {
         show_filtre=true;
       }); 
      },
      
      child:
    Container(
        width: 27,
        height: 27,
        decoration: new BoxDecoration(
                               border: new Border.all(color: Colors.white, width: 2),

                                    shape: BoxShape.circle,

          color: Color(0xff25a996),
		boxShadow: [BoxShadow(
        color: Color(0x2b2d2d2d),
        offset: Offset(0,4),
        blurRadius: 10,
        spreadRadius: 0
    ) ],
        ),
        child: Padding(padding: EdgeInsets.all(5),child:SvgPicture.asset(("assets/icon/mic-3-filtre.svg")))
      )))),*/
                if (id_filtre != null && status() == "Stop")
                  Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () {
                                      setState(() {
                                        show_filtre = true;
                                        id_filtre = null;
                                        url_filtre=null;
                                      });
                                    },
                                    child: Container(
                                        width: 27,
                                        height: 27,
                                        decoration: new BoxDecoration(
                                          border: new Border.all(
                                              color: Colors.white, width: 2),
                                          shape: BoxShape.circle,
                                          color: Color(0xffffffff),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0x2b2d2d2d),
                                                offset: Offset(0, 4),
                                                blurRadius: 10,
                                                spreadRadius: 0)
                                          ],
                                        ),
                                        child: Padding(
                                            padding: EdgeInsets.all(5),
                                            child: SvgPicture.asset(
                                              ("assets/icon/mic-3-filtre.svg"),
                                              color: Color(0xff25a996),
                                            )))),
                                SizedBox(
                                  width:
                                      (MediaQuery.of(context).size.width / 2) -
                                          65,
                                ),
                                new GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () {
                                      if(widget.idReceivers!=null && widget.id_conv!=null){
                                                           getToken().then((token)=>{
sendMessage(token, id_filtre, widget.idReceivers,
                              duree, widget.id_conv)
                                                           } );

                                      }
                                      else{
 Navigator.pushNamed(
                                          context, "/listsuggestions",
                                          arguments: {
                                            'duree': duree,
                                            'id_filtre': id_filtre
                                          });
                                      }
                                     
                                    },
                                    child: Container(
                                        width: 54,
                                        height: 54,
                                        decoration: new BoxDecoration(
                                          border: new Border.all(
                                              color: Colors.white, width: 2),
                                          shape: BoxShape.circle,
                                          color: Color(0xff25a996),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0x2b2d2d2d),
                                                offset: Offset(0, 8),
                                                blurRadius: 20,
                                                spreadRadius: 0)
                                          ],
                                        ),
                                        child: Padding(
                                            padding: EdgeInsets.all(11),
                                            child: SvgPicture.asset(
                                                ("assets/icon/arrow-right.svg")))))
                              ]))),
                if (!show_filtre && status() == "Stop")
                  Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () {
                                      setState(() {
                                        show_filtre = true;
                                      });
                                    },
                                    child: Container(
                                        width: 27,
                                        height: 27,
                                        decoration: new BoxDecoration(
                                          border: new Border.all(
                                              color: Colors.white, width: 2),
                                          shape: BoxShape.circle,
                                          color: Color(0xff25a996),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0x2b2d2d2d),
                                                offset: Offset(0, 4),
                                                blurRadius: 10,
                                                spreadRadius: 0)
                                          ],
                                        ),
                                        child: Padding(
                                            padding: EdgeInsets.all(5),
                                            child: SvgPicture.asset(
                                                ("assets/icon/mic-3-filtre.svg"))))),
                                SizedBox(
                                  width:
                                      (MediaQuery.of(context).size.width / 2) -
                                          65,
                                ),
                                new GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () {
                                       if(widget.idReceivers!=null && widget.id_conv!=null){
                                                           getToken().then((token)=>{
sendQucikMessage(token, base64Image, widget.idReceivers,
                              duree, widget.id_conv)
                                                           } );

                                      }
                                      else{
 Navigator.pushNamed(
                                          context, "/listsuggestions",
                                          arguments: {
                                            'duree': duree,
                                            'id_filtre': id_filtre,
                                            'file':base64Image
                                          });
                                      }
                                    },
                                    child: Container(
                                        width: 54,
                                        height: 54,
                                        decoration: new BoxDecoration(
                                          border: new Border.all(
                                              color: Colors.white, width: 2),
                                          shape: BoxShape.circle,
                                          color: Color(0xff25a996),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0x2b2d2d2d),
                                                offset: Offset(0, 8),
                                                blurRadius: 20,
                                                spreadRadius: 0)
                                          ],
                                        ),
                                        child: Padding(
                                            padding: EdgeInsets.all(11),
                                            child: SvgPicture.asset(
                                                ("assets/icon/arrow-right.svg")))))
                              ]))),
                if (show_filtre && status() == 'Stop' && id_filtre == null)
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                          margin: EdgeInsets.only(bottom: 10),
                          height: 100,
                          child: NotificationListener<ScrollNotification>(
                            onNotification: (ScrollNotification notification) {
                              if (notification is ScrollUpdateNotification) {
                                setState(() {
                                  page = pageController.page;
                                });
                              }
                            },
                            child: PageView.builder(
                              onPageChanged: (pos) {
                                setState(() {
                                  currentPage = pos;
                                });
                              },
                              physics: BouncingScrollPhysics(),
                              controller: pageController,
                              itemCount: listOfCharacters.length,
                              itemBuilder: (context, index) {
                                double scale;
                                if (currentPage == index) {
                                  scale = 52;
                                } else {
                                  scale = 27;
                                }
                                return circleOffer(
                                    listOfCharacters[index]['image'],
                                    listOfCharacters[index]['nom'],
                                    index,
                                    scale);
                              },
                            ),
                          ))),
              ])),
        ));
  }

  Future<void> sendMessage(String token, int id_voice, var id_reciver,
      String durre, String id_conv) async {
    FocusScope.of(context).requestFocus(FocusNode());

    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );

    data = ClientApi.sendMessage(token, id_voice, id_reciver, durre, id_conv);
    data
        .then((value) => {
              print(value.data),
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(AppLocalizations.of(context).translate('message_send')),
        
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false),
                  
            })
        .catchError((error) => {
              EasyLoading.showError(AppLocalizations.of(context).translate('error_try_again')),
              print("erreur : " + error.toString()),
              EasyLoading.dismiss()
            });
  }

sendQucikMessage(
      String token, String file, var id_reciver, String durre, String id_conv) {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );

    data = ClientApi.sendquickMessage(token, file, id_reciver, durre, id_conv);
    data
        .then((value) => {
              print(value.data),
              EasyLoading.dismiss(),
              EasyLoading.showSuccess(AppLocalizations.of(context).translate('message_send')),
 

            })
        .catchError((error) => {
              EasyLoading.showError(AppLocalizations.of(context).translate('error_try_again')),
              print("erreur11 : " + error.toString()),
              EasyLoading.dismiss(),
            });
  }

    Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  Future<void> lastFilteredVoice(String token, String id_voice) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    data = ClientApi.lastFilteredVoice(token,id_voice);
    data
        .then((value) => {
          EasyLoading.dismiss(),
              setState(() {
                url_filtre = value.data;
              }),
            })
        .catchError((error) =>
            {print("erreur : " + error.toString()), EasyLoading.dismiss()});
  }

  Widget circleOffer(String image, String text, int index, double scale) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          if (index == currentPage)
            new Text(text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Ubuntu',
                  color: Color(0xffffffff),
                  fontSize: 8,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0.2083333333333334,
                )),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
              onTap: () {
                if (index == currentPage)
                  _changer(
                      file, listOfCharacters[currentPage]['name'], currentPage);
              },
              child: Container(
                  height: scale,
                  width: scale,
                  decoration: new BoxDecoration(
                      border: new Border.all(color: Colors.white, width: 2),
                      shape: BoxShape.circle,
                      image: image == ""
                          ? null
                          : DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(image),
                            )))),
        ]);
  }

  _changer(File file, String filtre, int pos) {
    print("file:" + file.toString());
    print(filechange);
    if (filechange != null) {
      getToken().then((cleapi) {
        getFiltre(cleapi, filtre, base64Image);
      });
    }
  }



  Future<void> getFiltre(String token, String filtre_name, String voice) async {
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black;

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    );
    filtre = ClientApi.filterVoice(token, filtre_name, voice);
    filtre
        .then((value) => {
              print("photo : " + value.data.url),
              setState(() {
                id_filtre = value.data.tempData;
              }),
              print(id_filtre),
              openWebview(Endpoints.baseUrlSite + value.data.url),
            })
        .catchError((error) =>
            {print("erreur : " + error.toString()), EasyLoading.dismiss()});
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => _playerState = PlayerState.paused);
    return result;
  }

  Widget openWebview(String url) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Padding(
              // A simplified version of dialog.
              padding: EdgeInsets.fromLTRB(20, 100, 20, 100),
              child: Scaffold(
                  appBar: AppBar(
                    elevation: 0.0,
                    bottomOpacity: 0.0,
                    backgroundColor: Color(0xff3bc0ae),
                    leading: IconButton(
                      icon: Icon(Icons.close, color: Colors.white),
                      onPressed: () => {
                        getToken().then((token)=>{
                         lastFilteredVoice(token, id_filtre.toString())
                        }),
                        Navigator.of(context).pop(),}
                    ),
                  ),
                  body: WillPopScope(
                      onWillPop: () => _willPopCallback(_controller),
                      child: Center(
                        // Aligns the container to center

                        child: WebView(
                          key: UniqueKey(),
                          javascriptMode: JavascriptMode.unrestricted,
                          initialUrl: url,
                          onWebViewCreated:
                              (WebViewController webViewController) {
                            /* i am not sure what this line actually do */
                            _controller = webViewController;
                          },
                          onPageStarted: (String url) {
                            EasyLoading.dismiss();
                          },
                        ),
                      ))));
        });
  }

  Future<bool> _willPopCallback(WebViewController controller) async {
    controller
        .reload(); /* or you can use controller.reload() to just reload the page */
    return true;
  }

  Future<int> _stopPlayer() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        _playerState = PlayerState.stopped;
        _position = Duration();
      });
      _onComplete();
    }
    return result;
  }

  cancelMessage() {
    if (_playerState == PlayerState.paused ||
        _playerState == PlayerState.playing) {
      _stopPlayer();
    }
    _init();

    if (_timer != null) {
      _timer.cancel();
    }
    url_filtre=null;
  }

  _init() async {
    timer = "";

    start = 60;

    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String customPath = '/flutter_audio_recorder_';
        io.Directory appDocDirectory;
//        io.Directory appDocDirectory = await getApplicationDocumentsDirectory();
        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // .wav <---> AudioFormat.WAV
        // .mp4 .m4a .aac <---> AudioFormat.AAC
        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        _recorder =
            FlutterAudioRecorder(customPath, audioFormat: AudioFormat.WAV);

        await _recorder.initialized;
        // after initialization
        var current = await _recorder.current(channel: 0);
        print(current);
        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
          filechange = null;
          base64Image = null;
          id_filtre = null;
          duree = null;
          show_filtre = false;
        });
      } else {
        Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  _start() async {
    try {
      startTimer(60);
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      setState(() {
        _current = recording;
      });

      const tick = const Duration(seconds: 1);
      new Timer.periodic(tick, (Timer t) async {
        if (_currentStatus == RecordingStatus.Stopped) {
          t.cancel();
        }

        var current = await _recorder.current(channel: 0);
        // print(current.status);
        setState(() {
          _current = current;
          _currentStatus = _current.status;
          recorder = true;
          timer = _current?.duration.inMinutes.remainder(60).toString() +
              ":" +
              current?.duration.inSeconds.remainder(60).toString();
        });
      });
    } catch (e) {
      print(e);
    }
  }

  _stop() async {
    var result = await _recorder.stop();
    print("Stop recording: ${result.path}");
    filechange = result.path;
    print("Stop recording: ${result.duration}");
    file = widget.localFileSystem.file(result.path);
    print("File length: ${await file.length()}");
    List<int> imageBytes = file.readAsBytesSync();
    base64Image = base64Encode(imageBytes);
    print(base64Image);

    _timer.cancel();

    setState(() {
      _current = result;
      _currentStatus = _current.status;
      recorder = false;
      duree = _current.duration.inMinutes.remainder(60).toString() +
          ":" +
          _current.duration.inSeconds.remainder(60).toString();
      timer = "";
    });
    initPlayer(_current.path);
  }

  String status() {
    var text = "";
    switch (_currentStatus) {
      case RecordingStatus.Initialized:
        {
          text = 'Init';
          break;
        }
      case RecordingStatus.Recording:
        {
          text = 'Start';
          break;
        }
      case RecordingStatus.Paused:
        {
          text = 'Pause';
          break;
        }
      case RecordingStatus.Stopped:
        {
          text = 'Stop';
          break;
        }
      default:
        break;
    }
    return text;
  }

  Future<void> initPlayer(String url) async {
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);

    seekDone = false;

    // prepare the player with this audio but do not start playing
    await _audioPlayer.setReleaseMode(
        ReleaseMode.STOP); // set release mode so that it never releases

    _durationSubscription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);
    });

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = null;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() => _audioPlayerState = state);
    });
  }

  Future<int> _play(String path,bool isLocal) async {
    if (seekDone == true) {
      seekDone = false;
    }
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(path,
        position: playPosition, isLocal: isLocal);

    if (result == 1) {
      setState(() => _playerState = PlayerState.playing);
    }

    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
    _audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  void _onComplete() {
    setState(() => {
          _playerState = PlayerState.stopped,
          seekDone = true,
        });
  }
}


