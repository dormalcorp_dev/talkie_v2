import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/data.dart';
import '../data/network/Model/user.dart';


class ResetPassword3 extends StatefulWidget {
  @override
  _ResetPassword3State createState() => _ResetPassword3State();
}

class _ResetPassword3State extends State<ResetPassword3>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  Future<User> signup;
  FocusNode _focusNode = FocusNode();
  String passwordvalue = "";
  String repeatepasswordvalue = "";
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();
  Map arguments;
  String code;
  Future<Data> data;

  bool validationEqual(String currentValue, String checkValue) {
    if (currentValue == checkValue) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();

    super.dispose();
  }

  TextStyle style = TextStyle(
    fontFamily: 'Ubuntu',
    color: Color(0xffffffff),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
      if (arguments != null) {
        print(arguments['code']);
        code = arguments['code'];
      }
    }

    final passwordField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextFormField(
            controller: _pass,
            onChanged: (value) {
              setState(() {
                passwordvalue = value;
              });
            },
            style: style,
            obscureText: true,
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: "Mot de passe",
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final reptePasswordField = Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: TextFormField(
            style: style,
            obscureText: true,
            controller: _confirmPass,
            validator: (val) {
              if (val.isEmpty) return AppLocalizations.of(context).translate('required_field');
              if (val != _pass.text)
                return AppLocalizations.of(context).translate('password_mismatch');
              return null;
            },
            onChanged: (text) {
              _form.currentState.validate();
            },
            decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              labelText: AppLocalizations.of(context).translate('reenter_password'),
              labelStyle: TextStyle(
                fontFamily: 'Ubuntu',
                color: Color(0xffffffff),
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            )));

    final signupButon = Padding(
        padding: EdgeInsets.fromLTRB(38, 71, 37, 0),
        child: SizedBox(
            width: double.infinity, // match_parent
            height: 45,
            child: RaisedButton(
                elevation: 0,
                color: ( validateNumbercaracter(passwordvalue) &&
                        /*validateSpecialcaracter(passwordvalue) &&*/
                        validatelengthCaracter(passwordvalue) &&
                        validatecaracter(passwordvalue))
                    ? Colors.white
                    : Color(0xff6cc5b8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
                onPressed: () {
                  if (validateNumbercaracter(passwordvalue) &&
                      /*validateSpecialcaracter(passwordvalue) &&*/
                      validatelengthCaracter(passwordvalue) &&
                      validatecaracter(passwordvalue) &&
                      _form.currentState.validate()) {
                    print(code);
                    EasyLoading.instance
                      ..loadingStyle = EasyLoadingStyle.custom
                      ..backgroundColor = Colors.white
                      ..progressColor = Colors.white
                      ..indicatorType = EasyLoadingIndicatorType.wave
                      ..indicatorColor = Color(0xff3bc0ae)
                      ..textColor = Color(0xff3bc0ae)
                      ..userInteractions = false
                      ..maskType = EasyLoadingMaskType.black;
                    EasyLoading.show(
                      status: AppLocalizations.of(context).translate('loading'),
                    );

                    data = ClientApi.resetApi(code, _pass.text);
                    data
                        .then((value) => {
                              EasyLoading.instance.loadingStyle =
                                  EasyLoadingStyle.light,
                              EasyLoading.dismiss(),
                              Fluttertoast.showToast(
                                  msg: value.data,
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                 
                                  backgroundColor: Colors.black,
                                  textColor: Colors.white,
                                  fontSize: 16.0),
                              Navigator.pushNamedAndRemoveUntil(
                                  context, "/login", (r) => false)
                            })
                        .catchError((error) => {
                              EasyLoading.dismiss(),
                              EasyLoading.instance.loadingStyle =
                                  EasyLoadingStyle.custom,
                              EasyLoading.instance.backgroundColor = Colors.red,
                              EasyLoading.instance.indicatorColor =
                                  Colors.white,
                              EasyLoading.instance.progressColor = Colors.white,
                              EasyLoading.instance.textColor = Colors.white,
                              EasyLoading.showError(error)
                            });
                  }
                },
                child: Text(AppLocalizations.of(context).translate('send'),
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      color: Color(0xff25a996),
                      fontSize: 15,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )))));

    return Scaffold(
      backgroundColor: Color(0xff25a996),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xff25a996),
      ),
      body: Stack(children: <Widget>[
        SingleChildScrollView(
          child: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onPanDown: (_) {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            behavior: HitTestBehavior.translucent,
            child: Form(
              key: _form,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 172.0,
                    child: Image.asset(
                      "assets/logo.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.only(top: 0, right: 52, left: 18),
                          child: Text(AppLocalizations.of(context).translate('forgot_your_password'),
                              style: TextStyle(
                                fontFamily: 'Salome',
                                color: Color(0xffffffff),
                                fontSize: 28,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding:
                              EdgeInsets.only(top: 15, right: 52, left: 18),
                          child:
                              Text(AppLocalizations.of(context).translate('enter_new_password'),
                                  style: TextStyle(
                                    fontFamily: 'Ubuntu',
                                    color: Color(0xffffffff),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )))),
                  passwordField,
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(29, 15, 55, 0),
                          child: Text(
                              validatelengthCaracter(passwordvalue)
                                  ? "\u{2714} 8 "+AppLocalizations.of(context).translate('min_char')
                                  : "\u{2716} 8 "+AppLocalizations.of(context).translate('min_char'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xffffffff),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(29, 0, 55, 0),
                          child: new Text(
                              validatecaracter(passwordvalue)
                                  ? "\u{2714} 1 "+AppLocalizations.of(context).translate('letter')
                                  : "\u{2716} 1 "+AppLocalizations.of(context).translate('letter'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xffffffff),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(29, 0, 55, 0),
                          child: new Text(
                              validateNumbercaracter(passwordvalue)
                                  ? "\u{2714} 1 "+AppLocalizations.of(context).translate('number')
                                  : "\u{2716} 1 "+AppLocalizations.of(context).translate('number'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xffffffff),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
                  /*Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(29, 0, 55, 0),
                          child: new Text(
                              validateSpecialcaracter(passwordvalue)
                                  ? "\u{2714} 1 caractère spécial"
                                  : "\u{2716} 1 caractère spécial",
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xffffffff),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),*/
                  reptePasswordField,
                  signupButon,
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  bool validatelengthCaracter(String value) {
    if (value.length < 8) {
      return false;
    } else {
      return true;
    }
  }

  bool validateSpecialcaracter(String value) {
    String pattern = r'[!@#$%^&*(),.?":{}|<>]';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  bool validateNumbercaracter(String value) {
    String pattern = r'^(?=.*?[0-9])';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  bool validatecaracter(String value) {
    String pattern = r'^(?=.*[a-z])';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }
}
