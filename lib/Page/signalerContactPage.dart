import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:voicechanger/app_localizations.dart';
import 'package:voicechanger/data/network/ClientApi.dart';
import 'package:voicechanger/data/network/Model/data.dart';

class SignalerContactPage extends StatefulWidget {
  SignalerContact createState() => SignalerContact();
}

class SignalerContact extends State<SignalerContactPage> {
  TextEditingController motifController = new TextEditingController();
 String username;
    int id;
  Future<Data> data;

void onValueChange() {
    setState(() {
      motifController.text;
    });
  }

  @override
  void initState() {
    super.initState();
    motifController.addListener(onValueChange);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map arguments;
   
    if (ModalRoute.of(context) != null) {
      arguments = ModalRoute.of(context).settings.arguments as Map;
    }

  if (arguments != null) {
      print(arguments['username']);
      username = arguments['username'];
      print(arguments['id']);
      id = arguments['id'];
    }
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
          backgroundColor: Color(0xfff8f9f9),
          appBar: AppBar(
                    backgroundColor: Color(0xff25a996),
            elevation: 0,
            centerTitle: true,
            title: Text(AppLocalizations.of(context).translate('report_contact'),
                style: TextStyle(
                  fontFamily: 'Roboto',
                  color: Color(0xffffffff),
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                )),
          ),
          resizeToAvoidBottomInset: false,
          body: SingleChildScrollView(
              child: Column(children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding:
                              EdgeInsets.only(top: 15, right: 8, left: 8),
                          child: Text(
                              AppLocalizations.of(context).translate('why_reporting'),
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                color: Color(0xff000000),                                
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              )))),
              Padding(
                padding: EdgeInsets.fromLTRB(8, 35, 8, 0),
                child: TextField(
                  controller: motifController,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      hintText: AppLocalizations.of(context).translate('reason'),
                      labelText: AppLocalizations.of(context).translate('reason'),
                      counter: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                              height: 38,
                              width: MediaQuery.of(context).size.width,
                              decoration:
                                  new BoxDecoration(color: Color(0xffdddddd)),
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(15, 7, 0, 11),
                                  child: Text(
                                      "${140 - motifController.text.length} "+AppLocalizations.of(context).translate('remaining_characters'),
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: 'Ubuntu',
                                        color: Color(0xff000000),
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      ))))),
                      labelStyle: TextStyle(
                        fontFamily: 'Ubuntu',
                        color: Color(0xff000000),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                      ),
                      hintStyle: TextStyle(
                        fontFamily: 'Ubuntu',
                        color: Color(0xff555555),
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                      )),
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  minLines: 5,
                  maxLength: 140,
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(8, 35, 8, 0),
                child: ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width,
                    height: 45,
                    child: RaisedButton(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color:(motifController.text.isNotEmpty) ?  Color(0xff25a996):Colors.white),
                          borderRadius: BorderRadius.circular(4)),
                      color: (motifController.text.isNotEmpty) ? Colors.white:  Color(0xff6cc5b8),
                      child: Text(AppLocalizations.of(context).translate('send'),
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            color: (motifController.text.isNotEmpty) ?  Color(0xff25a996):Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          )),
                      onPressed: (motifController.text.isNotEmpty) ? () {
                        print(motifController.text);
                      print(id);
                      print(username);
                      signalerContact(motifController.text,id);
                      }: null,
                    )))
          ])),
        
        ));
// Allows you to get a list of all the ItemTags
  }



  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("apikey");
  }

  signalerContact( String motif, int id) async {
        getToken().then((token)=>{

    
    EasyLoading.instance
      ..loadingStyle = EasyLoadingStyle.custom
      ..backgroundColor = Colors.white
      ..progressColor = Colors.white
      ..indicatorType = EasyLoadingIndicatorType.wave
      ..indicatorColor = Color(0xff3bc0ae)
      ..textColor = Color(0xff3bc0ae)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.black,

    EasyLoading.show(
      status: AppLocalizations.of(context).translate('loading'),
    ),
    
    data = ClientApi.signalerUser(token, id,motif),
    data
        .then((value) => {
              EasyLoading.dismiss(),
               Fluttertoast.showToast(
                              msg: value.data,
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,

                              backgroundColor: Colors.black,
                              textColor: Colors.white,
                              fontSize: 16.0),
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false,
                  ),
       


             
            })
        .catchError((error) =>
            {EasyLoading.dismiss(),Fluttertoast.showToast(
                              msg: error.toString(),
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,
                              backgroundColor: Colors.black,
                              textColor: Colors.white,
                              fontSize: 16.0), print("erreur : " + error.toString())}),
  });
  }
}